export default [
    {
        idMeal: "52839",
        strMeal: "Chilli prawn linguine",
        strYoutube: "SC17Mc70Db0" //https://www.youtube.com/watch?v=SC17Mc70Db0"
    },
    {
        idMeal: "52840",
        strMeal: "Clam chowder",
        strYoutube: "fEN_fm6kX6k" // "https://www.youtube.com/watch?v=fEN_fm6kX6k"
    },
    {
        idMeal: "52841",
        strMeal: "Creamy Tomato Soup",
        strYoutube: "lBhwjjUiFk4" //https://www.youtube.com/watch?v=lBhwjjUiFk4"
    },
    {
        idMeal: "52842",
        strMeal: "Broccoli & Stilton soup",
        strYoutube:"_HgVLpmNxTY" // "https://www.youtube.com/watch?v=_HgVLpmNxTY"
    }, {
        idMeal: "52843",
        strMeal: "Lamb Tagine",
        strYoutube: "bR5Cqu84S_k" // "https://www.youtube.com/watch?v=bR5Cqu84S_k"
    }, {
        idMeal: "52844",
        strMeal: "Lasagne",
        strYoutube: "gfhfsBPt46s" //"https://www.youtube.com/watch?v=gfhfsBPt46s"
    }, {
        idMeal: "52845",
        strMeal: "Turkey Meatloaf",
        strYoutube: "mTvlmY4vCug" //"https://www.youtube.com/watch?v=mTvlmY4vCug"
    }, {
        idMeal: "52846",
        strMeal: "Chicken & mushroom Hotpot",
        strYoutube: "bXKWu4GojNI" //"https://www.youtube.com/watch?v=bXKWu4GojNI"
    }, {
        idMeal: "52847",
        strMeal: "Pork Cassoulet",
        strYoutube: "MEdHMTD0VCA" //"https://www.youtube.com/watch?v=MEdHMTD0VCA"
    }, {
        idMeal: "52848",
        strMeal: "Bean & Sausage Hotpot",
        strYoutube: "B0YX0yPX4Wo" //"https://www.youtube.com/watch?v=B0YX0yPX4Wo"
    }, {
        idMeal: "52849",
        strMeal: "Spinach & Ricotta Cannelloni",
        strYoutube: "rYGaEJjyLQA" //"https://www.youtube.com/watch?v=rYGaEJjyLQA"
    }, {
        idMeal: "52850",
        strMeal: "Chicken Couscous",
        strYoutube:  "GZQGy9oscVk" //"https://www.youtube.com/watch?v=GZQGy9oscVk"
    }, {

        idMeal: "52851",
        strMeal: "Nutty Chicken Curry",
        strYoutube: "nSQNfZxOdeU" //"https://www.youtube.com/watch?v=nSQNfZxOdeU"
    }
]

