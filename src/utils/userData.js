export default [
    {
        userUid : '1',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/63.jpg",
        userName : 'Olivia',
        recipePhoto : 'https://picturecorrect-wpengine.netdna-ssl.com/wp-content/uploads/2014/05/professional-food-photos-1.jpg',
    },
    {
        userUid : '2',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/14.jpg",
        userName : 'Emma',
        recipePhoto : 'https://i.ytimg.com/vi/mRRt_qIarBg/maxresdefault.jpg',
    },
    {
        userUid : '3',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/26.jpg",
        userName : 'Ava',
        recipePhoto : 'https://www.photographers-online.com/wp-content/uploads/2019/03/Screen-Shot-2019-03-07-at-23.35.49.jpg',
    },
    {
        userUid : '4',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/27.jpg",
        userName : 'Sophia',
        recipePhoto : 'https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/129848472/original/34760c0fe9d6ee04c5090051a5cd6cfae3409447/shoot-quality-food-photography-for-you.jpg',
    },
    {
        userUid : '5',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/29.jpg",
        userName : 'Isabella',
        recipePhoto : 'https://eyeq.photos/wp-content/uploads/2018/07/chocolate-delicious-dessert-574111-e1532384590469.jpg',
    },
    {
        userUid : '6',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/30.jpg",
        userName : 'Charlotte',
        recipePhoto : 'https://www.hotkhana.com/images/Creatives/Creatives_Strategy/BMP3.jpg',
    },
    {
        userUid : '7',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/31.jpg",
        userName : 'Amelia',
        recipePhoto : 'https://glmastudio.com/wp-content/uploads/2018/09/03.jpg',
    },
    {
        userUid : '8',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/32.jpg",
        userName : 'Mia',
        recipePhoto : 'https://lh3.googleusercontent.com/proxy/xFjJDMu2Ud-431Zhc0ctkSdl5J7uK7pEKrab7PPLDqvmUrP7zdqoYgqxUcYcZHuohePng-wPU_D38GZII4nlyoldXe9aorXHnP88uiLHupGIYTXyKdJ6m08ftcQHkrsDclOvenvNu_h-1u5JkA',
    },
    {
        userUid : '9',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/33.jpg",
        userName : 'Emily',
        recipePhoto : 'https://nice-branding.com/wp-content/uploads/2019/08/NBA-recipePhotography-Blog-Images5-1024x650.png',
    },
    {
        userUid : '10',
        userPhotoUrl : "https://randomuser.me/api/portraits/women/34.jpg",
        userName : 'Camila',
        recipePhoto : 'https://lh3.googleusercontent.com/proxy/zS4JkhSRS4gcwbM-u_VF6GwvfF93qw-l85zzYAttcrEquYaldD7Kql_QVeqowq7RvY429HfkjaVofDxaP28',
    },


]