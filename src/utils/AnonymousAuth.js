import auth from '@react-native-firebase/auth';

const signIn = () => {
    auth()
        .signInAnonymously()
        .then(() => {
            console.log('User signed in anonymously');
        })
        .catch((error) => {
            if (error.code === 'auth/operation-not-allowed') {
                console.log('Enable anonymous in your firebase console.');
            }
            console.log('ANONYMOUS SIGN IN ERROR ', error);
        });
};

const signOut = () => {
    auth()
        .signOut()
        .then(() => console.log('User signed out!'));
};

export {signIn, signOut};
