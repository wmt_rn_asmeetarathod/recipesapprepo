export const Color = {
    /*Main Theme Colors*/
    PRIMARY: '#00bcd4',
    PRIMARY_DARK: '#01b2c9',
    ACCENT_COLOR: '#00bcd4',
    /*Text Colors*/
    TEXT_PRIMARY: '#000000',
    TEXT_SECONDARY: '#696969',
    TEXT_PLACEHOLDER: '#a8a8a8',
    TEXT_COLOR_ACTIVE: '#00bcd4',
    /*Other Colors*/
    PRIMARY_BACKGROUND: '#EEEEEE',
    DIVIDER_COLOR: 'rgba(255,255,255,0.2)',
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    DARK_LIGHT_BLACK: 'rgba(0, 0, 0, 0.5)',
    CELLO : "#334B5C",
    YELLOW : "#FAD702",
    RED : "#F71106",
    BLUE : "#458eff",
    GRAY01 : "#4B4B4B",
    GRAY02 : "#1C1C1C",
    YELLOW01 : "#F9B601",
    PHOTO_BOX : "#CCD1D1"

};
