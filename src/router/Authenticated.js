import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import Routes from 'src/router/Routes';
import {Color, Strings, ThemeUtils} from 'src/utils';

// Screens Name
import FoodList from 'src/screens/Authenticated/FoodListScreen';
import FoodDetail from 'src/screens/Authenticated/FoodDetailsScreen';
import CategoryList from 'src/screens/Authenticated/CategoryListScreen';
import Videos from 'src/screens/Authenticated/VideosScreen';
import Profile from 'src/screens/Authenticated/ProfileScreen';
import CreateRecipe from 'src/screens/Authenticated/CreateRecipeScreen';
import FeedHome from 'src/screens/Authenticated/FeedHomeScreen';
import UserDetails from 'src/screens/Authenticated/UserDetailsScreen';
import UserList from 'src/screens/Authenticated/UserListScreen';

import TabNavigator from './MainTab';

const Stack = createStackNavigator();

const navigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={__DEV__ ? Routes.Home : Routes.Home}
            screenOptions={({navigation}) => ({
                headerTitleAlign: 'center',
                headerTitleStyle: {
                    fontFamily: ThemeUtils.FontStyle.regular,
                },
            })}>
            <Stack.Screen
                name={Routes.Home}
                component={TabNavigator}
                options={{
                    headerShown: true,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                    headerTitleAlign: 'left',
                }}
            />
            <Stack.Screen
                name={Routes.CategoryList}
                component={CategoryList}
                options={({route, navigation}) => ({
                    title: Strings.categories,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                })}
            />
            <Stack.Screen
                name={Routes.Videos}
                component={Videos}
                options={({route, navigation}) => ({
                    title: Strings.videos,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                    headerShown: false,
                })}
            />
            <Stack.Screen
                name={Routes.Profile}
                component={Profile}
                options={({route, navigation}) => ({
                    title: Strings.profile,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                    headerShown: false,
                })}
            />
            <Stack.Screen
                name={Routes.UserDetails}
                component={UserDetails}
                options={({route, navigation}) => ({
                    title: 'UserDetails',
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                })}
            />
            <Stack.Screen
                name={Routes.CreateRecipe}
                component={CreateRecipe}
                options={({route, navigation}) => ({
                    title: Strings.createRecipe,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                })}
            />
            <Stack.Screen
                name={Routes.FeedHome}
                component={FeedHome}
                options={({route, navigation}) => ({
                    title: Strings.home,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                    headerShown: true,
                })}
            />
            <Stack.Screen
                name={Routes.FoodList}
                component={FoodList}
                options={({route, navigation}) => ({
                    title: route.params.categoryName,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                })}
            />
            <Stack.Screen
                name={Routes.FoodDetail}
                component={FoodDetail}
                options={({route, navigation}) => ({
                    title: route.params.itemName,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                })}
            />
            <Stack.Screen
                name={Routes.UserList}
                component={UserList}
                options={({route, navigation}) => ({
                    title: route.params.title,
                    headerStyle: {
                        backgroundColor: Color.CELLO,
                    },
                })}
            />
        </Stack.Navigator>
    );
};

export default navigator;
