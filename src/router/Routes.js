const Routes = {
    /*  Root Stacks    */

    Authenticated: 'Authenticated',
    UnAuthenticated: 'UnAuthenticated',
    Splash: 'Splash',

    /*  Non-Authenticated Routes    */
    Login: 'Login',

    /*  Authenticated Routes    */

    Home: 'Home',
    FoodList: 'FoodList',
    FoodDetail: 'FoodDetail',
    CategoryList: 'CategoryList',
    Videos: 'Videos',
    Profile : 'Profile',
    CreateRecipe : 'CreateRecipe',
    FeedHome : 'FeedHome',
    UserDetails : 'UserDetails',
    SignUp : 'SignUp',
    UserList : 'UserList',
};
export default Routes;

