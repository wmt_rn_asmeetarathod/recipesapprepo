import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Color, Strings, ThemeUtils} from 'src/utils';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';

import Routes from 'src/router/Routes';

import CategoryList from 'src/screens/Authenticated/CategoryListScreen';
import Videos from 'src/screens/Authenticated/VideosScreen';
import Profile from 'src/screens/Authenticated/ProfileScreen';
import FeedHome from 'src/screens/Authenticated/FeedHomeScreen';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName={Routes.FeedHome}
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    let iconColor;

                    if (route.name === Routes.FeedHome) {
                        iconName = focused ? 'home' : 'home-outline';
                        iconColor = focused
                            ? Color.WHITE
                            : Color.DARK_LIGHT_BLACK;
                    } else if (route.name === Routes.CategoryList) {
                        iconName = focused ? 'fast-food' : 'fast-food-outline';
                        iconColor = focused
                            ? Color.WHITE
                            : Color.DARK_LIGHT_BLACK;
                    } else if (route.name === Routes.Videos) {
                        iconName = focused ? 'videocam' : 'videocam-outline';
                        iconColor = focused
                            ? Color.WHITE
                            : Color.DARK_LIGHT_BLACK;
                    } else if (route.name === Routes.Profile) {
                        iconName = focused ? 'person' : 'person-outline';
                        iconColor = focused
                            ? Color.WHITE
                            : Color.DARK_LIGHT_BLACK;
                    }

                    return (
                        <IonIcon name={iconName} size={30} color={iconColor} />
                    );
                },
            })}
            tabBarOptions={{
                activeTintColor: Color.WHITE,
                inactiveTintColor: Color.DARK_LIGHT_BLACK,
                activeBackgroundColor: Color.CELLO,
                inactiveBackgroundColor: Color.WHITE,
                style: {
                    backgroundColor: Color.WHITE,
                },
            }}>
            <Tab.Screen
                name={Routes.FeedHome}
                component={FeedHome}
                options={{title: Strings.home}}
            />
            <Tab.Screen
                name={Routes.CategoryList}
                component={CategoryList}
                options={{title: Strings.categories}}
            />
            <Tab.Screen name={Routes.Videos} component={Videos} />
            <Tab.Screen name={Routes.Profile} component={Profile} />
        </Tab.Navigator>
    );
};

export default TabNavigator;
