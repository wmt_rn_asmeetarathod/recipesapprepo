import React, {useEffect, useState} from 'react';
import {View, Modal} from 'react-native';
import PropTypes from 'prop-types';
import LottieView from 'lottie-react-native';
import styles from './styles';
import LOADER from 'src/assets/animation/loading.json';

const Loader = (props) => {
    const {loading} = props;
    return (
        <>
            <Modal
                transparent={props?.transparent}
                animationType={props?.animationType}
                visible={loading}>                
                    <View style={styles.lottieView}>
                        <LottieView
                            source={LOADER}
                            autoPlay
                            loop
                            style={styles.lottieViewStyle}
                        />
                    </View>
                
            </Modal>
        </>
    );
};

Loader.defaultProps = {
    animationType: 'none',
    transparent: true,
};

Loader.propTypes = {
    animationType: PropTypes.string,
    transparent: PropTypes.bool,
};

export default Loader;
