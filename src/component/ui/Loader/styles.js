import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export default StyleSheet.create({
    lottieView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    lottieViewStyle: {
        height: ThemeUtils.relativeRealWidth(30),
        width: ThemeUtils.relativeRealWidth(30),

        resizeMode: 'cover',
    },
});
