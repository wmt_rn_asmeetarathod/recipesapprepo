import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'yellow',
        justifyContent: 'center',
        // height : ThemeUtils.relativeHeight(100),
        alignSelf: 'flex-end',
    },
    modal: {
        justifyContent: 'center',
        backgroundColor: 'purple',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
    },
    modalParentContainer: {
        justifyContent: 'flex-end',
        flexGrow :1,
    },  
    modalContainer: {
        backgroundColor: Color.WHITE,
        paddingHorizontal: 16,
        paddingTop: 20,
        paddingBottom: 40,
        alignItems: 'center',
    },
    modalTitle: {
        fontSize: 22,
        fontWeight: '600',
    },
    modalText: {
        fontSize: 18,
        color: '#555',
        marginTop: 14,
        textAlign: 'center',
        marginBottom: 10,
    },
    button: {
        backgroundColor: Color.BLACK,
        paddingVertical: 12,
        paddingHorizontal: 16,
        width: '100%',
        alignItems: 'center',
        marginTop: 10,
    },
    buttonText: {
        color: '#fff',
        fontSize: 20,
    },
});
