import React, {useEffect, useState} from 'react';
import {
    View,
    FlatList,
    Image,
    ImageBackground,
    Share,
    Modal,
    TextInput,
    TouchableOpacity,
    Text,
    ActivityIndicator,
} from 'react-native';
import Routes from 'src/router/Routes';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {styles} from './styles';
import {Label, Ripple} from 'src/component';
import {Color, Strings} from 'src/utils';
import LinearGradient from 'react-native-linear-gradient';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';
import USER_PLACEHOLDER from 'src/assets/images/user_placeholder_image.png';
import Voice from '@react-native-voice/voice';
import {connect} from 'react-redux';

const Button = ({children, ...props}) => (
    <TouchableOpacity style={styles.button} {...props}>
        <Label style={styles.buttonText}>{children}</Label>
    </TouchableOpacity>
);

const NoInternetModal = (props) => {
    const {show, onRetry, isRetrying, visible} = props;

    const onPress = () => {
        if (props.onPress) {
            props.onPress();
        }
    };

    return (
        <View style={styles.mainContainer}>
            <Modal
                isVisible={show}
                style={styles.modal}
                animationInTiming={600}>
                <View style={styles.modalParentContainer}>
                    <View style={styles.modalContainer}>
                        <Label style={styles.modalTitle}>
                            {Strings.connectionError}
                        </Label>
                        <Label style={styles.modalText}>
                            {Strings.deviceNotConnectedToInternet}
                        </Label>
                        <Button onPress={onPress} disabled={isRetrying}>
                            {visible ? (
                                <ActivityIndicator
                                    color={Color.WHITE}
                                    size={'large'}
                                />
                            ) : (
                                Strings.tryAgain
                            )}
                        </Button>
                    </View>
                </View>
            </Modal>
        </View>
    );
};
export default NoInternetModal;
