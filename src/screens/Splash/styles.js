import {StyleSheet} from 'react-native';
import {Color} from 'src/utils';

export const styles = StyleSheet.create({
    vwLogo: {
        width: '55%',
        height: '50%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        width: '50%',
        height: '30%',
        backgroundColor: Color.WHITE,
        borderRadius: 50,
    },
    mainContainer: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.WHITE,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'contain',
        zIndex: -2,
        width: null,
        height: null,
        opacity: 0.6,
    },
});
