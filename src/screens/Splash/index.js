import React, {useEffect, useState} from 'react';

import {View, Image} from 'react-native';
import auth from '@react-native-firebase/auth';
import {styles} from './styles';
import {IS_IOS, isNetworkConnected} from 'src/utils';
import Routes from 'src/router/Routes';

import {CommonActions} from '@react-navigation/native';
import {connect} from 'react-redux';
import FOOD_LOGO from 'src/assets/images/food_logo.png';

const Splash = (props) => {
    const user = auth().currentUser;
    console.log('Splash user ==>> ', user);

    // navigate to Authenticated route
    const resetToAuth = CommonActions.reset({
        index: 0,
        routes: [
            {
                name: Routes.Authenticated,
            },
        ],
    });

    // navigate to UnAuthenticated route
    const resetToNotAuth = CommonActions.reset({
        index: 0,
        routes: [
            {
                name: Routes.UnAuthenticated,
            },
        ],
    });

    /*  Life-cycles Methods */
    useEffect(() => {
        let splashDelay = IS_IOS ? 100 : 1000;

        if (!user) {
            setTimeout(() => {
                props.navigation.dispatch(resetToNotAuth);
            }, splashDelay);
        } else {
            setTimeout(() => {
                props.navigation.dispatch(resetToAuth);
            }, splashDelay);
        }
    }, []);

    return (
        <View style={styles.mainContainer}>
            <View style={styles.vwLogo}>
                <Image source={FOOD_LOGO} style={styles.logo} />
            </View>
        </View>
    );
};

// set store values as props
const mapStateToProps = (state) => {
    return {user: state.user, token: state.token};
};

// const mapDispatchToProps = (dispatch) => {
//     return {};
// };

export default connect(mapStateToProps)(Splash);
