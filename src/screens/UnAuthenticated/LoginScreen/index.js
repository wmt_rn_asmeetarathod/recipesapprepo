import React, {useEffect, useState} from 'react';
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import email from 'react-native-email';
import {
    View,
    Image,
    TextInput,
    ImageBackground,
    ToastAndroid,
    Modal,
} from 'react-native';
import {RoundButton, Label, Ripple, NoInternetModal} from 'src/component';
import {Controller, useForm} from 'react-hook-form';
import {
    CommonStyle,
    Constants,
    Messages,
    ThemeUtils,
    Color,
    Strings,
    isNetworkConnected,
    // anonymousSignIn,
} from 'src/utils';
import styles from './styles';
import Routes from 'src/router/Routes';
import {CommonActions} from '@react-navigation/native';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';
import FOOD_LOGO from 'src/assets/images/food_logo.png';

const Login = (props) => {
    const {
        register,
        setValue,
        control,
        reset,
        clearErrors,
        getValues,
        errors,
        handleSubmit,
    } = useForm();
    const [loginIndicator, setLoginIndicator] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [onSubmitIndicator, setOnSubmitIndicator] = useState(false);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);

    /*  Life-cycles Methods */
    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    const checkNetwork = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setNoInternetModalVisible(false);
            }
        });
    };

    useEffect(() => {
        GoogleSignin.configure({
            webClientId:
                '200951270258-7i3pmmjr4rd93i6aukuvih7d5qqt5ui7.apps.googleusercontent.com',
        });
    }, []);

    /*  Public Interface Methods */
    /*  Validation Methods  */

    /*  UI Events Methods   */
    const goToSignUpScreen = () => {
        props.navigation.navigate(Routes.SignUp);
    };

    const openModal = () => {
        setModalVisible(true);
    };

    const onSubmit = (data) => {
        setOnSubmitIndicator(true);
        auth()
            .signInWithEmailAndPassword(data.email, data.password)
            .then((res) => {
                setOnSubmitIndicator(false);
                //  anonymousSignIn();
                props.navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [
                            {
                                name: Routes.Authenticated,
                                state: {
                                    routes: [
                                        {
                                            name: Routes.Home,
                                        },
                                    ],
                                },
                            },
                        ],
                    }),
                );
                ToastAndroid.show(Strings.loginSuccessfully, ToastAndroid.LONG);
            })
            .catch((error) => {
                setOnSubmitIndicator(false);
                if (error.code === 'auth/network-request-failed') {
                    setNoInternetModalVisible(true);
                } else {
                    ToastAndroid.show(
                        Strings.notMatchOurRecords,
                        ToastAndroid.LONG,
                    );
                }
            });
    };

    // Handle Login with Google
    const handleLoginWithGoogle = () => {
        loginWithGoogle()
            .then((res) => {
                setLoginIndicator(false);
                const user = auth().currentUser;
                const reference = database().ref(`/users/${user.uid}`);
                reference
                    .update({
                        fullName: user.displayName,
                        email: user.email,
                        photoURL: user.photoURL,
                        uid: user.uid,
                    })
                    .then(() =>
                        console.log(
                            'User Detail added to Database when login with google',
                        ),
                    )
                    .catch((e) =>
                        console.log(
                            'error while login with google when User Details ADD ==>> ',
                            e,
                        ),
                    );
                //  anonymousSignIn();
                props.navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [
                            {
                                name: Routes.Authenticated,
                                state: {
                                    routes: [
                                        {
                                            name: Routes.Home,
                                        },
                                    ],
                                },
                            },
                        ],
                    }),
                );
                ToastAndroid.show(Strings.loginSuccessfully, ToastAndroid.LONG);
            })
            .catch((error) => {
                setLoginIndicator(false);
                if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                    ToastAndroid.show(
                        Strings.cancelledLogin,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === statusCodes.IN_PROGRESS) {
                    ToastAndroid.show(
                        Strings.loginInProgress,
                        ToastAndroid.LONG,
                    );
                } else if (
                    error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE
                ) {
                    ToastAndroid.show(
                        Strings.playServiceNotAvailable,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === 'auth/network-request-failed') {
                    setNoInternetModalVisible(true);
                } else {
                    console.log('Error while login ==> ', error);
                    ToastAndroid.show(
                        Strings.somethingWentWrong,
                        ToastAndroid.LONG,
                    );
                }
            });
    };

    // Method for Login with Google
    const loginWithGoogle = async () => {
        await GoogleSignin.hasPlayServices();
        const {idToken, user} = await GoogleSignin.signIn();

        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        setLoginIndicator(true);
        return auth().signInWithCredential(googleCredential);
    };

    const handleForgotPassword = () => {
        const to = ['asmeetar@webmob.tech'];
        setModalVisible(false);
        email(to, {
            subject: 'React Native Recipe App',
            body: 'Recipes App , Learn how to Cook',
        })
            .then(() => {
                console.log('Email Sent');
            })
            .catch(console.error);
    };
    /*  Custom-Component sub-render Methods */

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={checkNetwork}
                />
            ) : (
                <View style={styles.mainContainer}>
                    <ImageBackground
                        style={styles.backgroundImage}
                        source={APP_BACKGROUND}>
                        {modalVisible && (
                            <View
                                style={styles.modalContainer}
                                // onTouchEnd={()=>setModalVisible(false)}
                            >
                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={modalVisible}
                                    onRequestClose={() =>
                                        setModalVisible(false)
                                    }>
                                    <View style={styles.vwUpper}>
                                        <View style={styles.modalView}>
                                            <Controller
                                                control={control}
                                                render={({
                                                    onChange,
                                                    onBlur,
                                                    value,
                                                }) => (
                                                    <>
                                                        <TextInput
                                                            placeholder={
                                                                Strings.email
                                                            }
                                                            style={
                                                                styles.inputFieldForgotPasswordStyle
                                                            }
                                                            placeholderTextColor={
                                                                Color.DARK_LIGHT_BLACK
                                                            }
                                                            value={value}
                                                            onChangeText={
                                                                onChange
                                                            }
                                                            onFocus={() =>
                                                                clearErrors(
                                                                    'forgotEmail',
                                                                )
                                                            }
                                                        />
                                                        {errors.forgotEmail
                                                            ?.message && (
                                                            <Label small>
                                                                {
                                                                    errors
                                                                        .forgotEmail
                                                                        ?.message
                                                                }
                                                            </Label>
                                                        )}
                                                    </>
                                                )}
                                                name={'forgotEmail'}
                                                defaultValue={''}
                                                rules={{
                                                    required:
                                                        Messages.Errors
                                                            .emailBlank,
                                                    pattern: {
                                                        value: Constants.Regex
                                                            .PASSWORD,
                                                        message:
                                                            Messages.Errors
                                                                .emailValidity,
                                                    },
                                                }}
                                            />
                                            <View style={styles.vwLoginButton}>
                                                <RoundButton
                                                    click={handleForgotPassword}
                                                    btn_sm
                                                    width={ThemeUtils.relativeWidth(
                                                        70,
                                                    )}
                                                    mt={ThemeUtils.relativeRealHeight(
                                                        4,
                                                    )}
                                                    backgroundColor={Color.BLUE}
                                                    textColor={Color.WHITE}
                                                    // visible={loginIndicator}
                                                    style={
                                                        styles.submitLabelStyle
                                                    }>
                                                    {Strings.submit}
                                                </RoundButton>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>
                        )}
                        <View style={styles.formContainer}>
                            <View style={CommonStyle.content_center}>
                                <Image
                                    style={styles.logoStyle}
                                    source={FOOD_LOGO}
                                />
                            </View>
                            <View style={styles.inputView}>
                                <Controller
                                    control={control}
                                    render={({onChange, onBlur, value}) => (
                                        <>
                                            <TextInput
                                                placeholder={Strings.email}
                                                style={styles.inputFieldStyle}
                                                placeholderTextColor={
                                                    Color.WHITE
                                                }
                                                value={value}
                                                onChangeText={onChange}
                                                onFocus={() =>
                                                    clearErrors('email')
                                                }
                                            />
                                            {errors.email?.message && (
                                                <Label
                                                    color={Color.WHITE}
                                                    small>
                                                    {errors.email?.message}
                                                </Label>
                                            )}
                                        </>
                                    )}
                                    name={'email'}
                                    defaultValue={''}
                                    rules={{
                                        required: Messages.Errors.emailBlank,
                                        pattern: {
                                            value: Constants.Regex.PASSWORD,
                                            message:
                                                Messages.Errors.emailValidity,
                                        },
                                    }}
                                />

                                <Controller
                                    control={control}
                                    render={({onChange, onBlur, value}) => (
                                        <>
                                            <TextInput
                                                placeholder={Strings.password}
                                                style={styles.inputFieldStyle}
                                                placeholderTextColor={
                                                    Color.WHITE
                                                }
                                                secureTextEntry
                                                value={value}
                                                onChangeText={onChange}
                                                onFocus={() =>
                                                    clearErrors('password')
                                                }
                                            />
                                            {errors.password?.message && (
                                                <Label
                                                    color={Color.WHITE}
                                                    small>
                                                    {errors.password?.message}
                                                </Label>
                                            )}
                                        </>
                                    )}
                                    name={'password'}
                                    defaultValue={''}
                                    rules={{required: Messages.Errors.pwdBlank}}
                                />
                            </View>
                            <View style={styles.vwLoginButton}>
                                <RoundButton
                                    click={handleSubmit(onSubmit)}
                                    width={ThemeUtils.relativeWidth(80)}
                                    mt={ThemeUtils.relativeRealHeight(4)}
                                    backgroundColor={Color.YELLOW01}
                                    textColor={Color.WHITE}
                                    visible={onSubmitIndicator}
                                    style={styles.submitLabelStyle}>
                                    {Strings.loginCaps}
                                </RoundButton>
                                <RoundButton
                                    click={handleLoginWithGoogle}
                                    width={ThemeUtils.relativeWidth(80)}
                                    mt={ThemeUtils.relativeRealHeight(4)}
                                    backgroundColor={Color.YELLOW01}
                                    textColor={Color.WHITE}
                                    visible={loginIndicator}
                                    style={styles.submitLabelStyle}>
                                    {Strings.loginWithGoogle}
                                </RoundButton>
                                <Ripple
                                    style={styles.signUpLabel}
                                    onPress={() => goToSignUpScreen()}>
                                    <Label color={Color.WHITE}>
                                        {Strings.signUpHere}
                                    </Label>
                                </Ripple>
                                <Ripple
                                    style={styles.signUpLabel}
                                    onPress={() => openModal()}>
                                    <Label color={Color.WHITE}>
                                        {Strings.forgotPassword}
                                    </Label>
                                </Ripple>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            )}
        </>
    );
};

export default Login;
