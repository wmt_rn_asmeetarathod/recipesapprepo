import React,{
    useEffect,
    useState,
} from 'react';
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
  } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import {
    View,
    Image,
    TextInput,
    ImageBackground,
    ToastAndroid,
} from 'react-native';
import {RoundButton, Label, NoInternetModal} from 'src/component';
import {
    Controller, 
    useForm
} from 'react-hook-form';
import {
    CommonStyle,
    Constants,
    Messages,
    ThemeUtils,
    Color,
    Strings,
    isNetworkConnected,
} from 'src/utils';
import styles from './styles';
import Routes from 'src/router/Routes';
import { CommonActions } from '@react-navigation/native';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';
import FOOD_LOGO from 'src/assets/images/food_logo.png';

const SignUp = (props) => {
    const {
        register,
        setValue,
        control,
        reset,
        clearErrors,
        getValues,
        errors,
        handleSubmit,
    } = useForm();
    const [indicator, setIndicator] = useState(false);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);

    /*  Life-cycles Methods */
    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    const checkNetwork = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setNoInternetModalVisible(false);
            }
        });
    };

    useEffect(() => {
        GoogleSignin.configure({
            webClientId:
                '200951270258-7i3pmmjr4rd93i6aukuvih7d5qqt5ui7.apps.googleusercontent.com',
        });
    }, []);

    /*  UI Events Methods   */
    const onSubmit = (data) => {
        if (data.password === data.confirmPassword) {
            handleSignUp(data.fullName, data.email, data.password);
        } else {
            ToastAndroid.show(Messages.Errors.pwdMisMatch, ToastAndroid.LONG);
        }
    };

    // Handle Login with Google
    const handleSignUp = (name, email, pswd) => {
        setIndicator(true);
        auth()
            .createUserWithEmailAndPassword(email, pswd)
            .then((res) => {
                setIndicator(false);
                console.log('User Created and Signed In!');
                const user = auth().currentUser;
                const reference = database().ref(`/users/${user.uid}`);
                reference
                    .update({
                        fullName: name,
                        email: user.email,
                        photoURL:
                            'https://randomuser.me/api/portraits/women/63.jpg',
                        uid: user.uid,
                        password: pswd,
                    })
                    .then(() =>
                        console.log(
                            'User Detail added to Database when Signup',
                        ),
                    )
                    .catch((e) =>
                        console.log(
                            'error while Signup when User Details ADD ==>> ',
                            e,
                        ),
                    );

                props.navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [
                            {
                                name: Routes.Authenticated,
                                state: {
                                    routes: [
                                        {
                                            name: Routes.Home,
                                        },
                                    ],
                                },
                            },
                        ],
                    }),
                );
                ToastAndroid.show(
                    Strings.signUpSuccessfully,
                    ToastAndroid.LONG,
                );
            })
            .catch((error) => {
                setIndicator(false);
                if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                    ToastAndroid.show(
                        Strings.cancelledLogin,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === statusCodes.IN_PROGRESS) {
                    ToastAndroid.show(
                        Strings.loginInProgress,
                        ToastAndroid.LONG,
                    );
                } else if (
                    error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE
                ) {
                    ToastAndroid.show(
                        Strings.playServiceNotAvailable,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === 'auth/network-request-failed') {
                    setNoInternetModalVisible(true);
                }
                else {
                    ToastAndroid.show(
                        Strings.somethingWentWrong,
                        ToastAndroid.LONG,
                    );
                }
            });
    };
    
    /*  Custom-Component sub-render Methods */

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={checkNetwork}
                />
            ) : (
                <View style={styles.mainContainer}>
                    <ImageBackground
                        style={styles.backgroundImage}
                        source={APP_BACKGROUND}>
                        <View style={styles.formContainer}>
                            <View style={CommonStyle.content_center}>
                                <Image
                                    style={styles.logoStyle}
                                    source={FOOD_LOGO}
                                />
                            </View>
                            <View style={styles.inputView}>
                                <Controller
                                    control={control}
                                    render={({onChange, onBlur, value}) => (
                                        <>
                                            <TextInput
                                                placeholder={Strings.fullName}
                                                style={styles.inputFieldStyle}
                                                placeholderTextColor={
                                                    Color.WHITE
                                                }
                                                value={value}
                                                onChangeText={onChange}
                                                onFocus={() =>
                                                    clearErrors('fullName')
                                                }
                                            />
                                            {errors.fullName?.message && (
                                                <Label
                                                    color={Color.WHITE}
                                                    small>
                                                    {errors.fullName?.message}
                                                </Label>
                                            )}
                                        </>
                                    )}
                                    name={'fullName'}
                                    defaultValue={''}
                                    rules={{
                                        required: Messages.Errors.fullNameBlank,
                                    }}
                                />
                                <Controller
                                    control={control}
                                    render={({onChange, onBlur, value}) => (
                                        <>
                                            <TextInput
                                                placeholder={Strings.email}
                                                style={styles.inputFieldStyle}
                                                placeholderTextColor={
                                                    Color.WHITE
                                                }
                                                value={value}
                                                onChangeText={onChange}
                                                onFocus={() =>
                                                    clearErrors('email')
                                                }
                                            />
                                            {errors.email?.message && (
                                                <Label
                                                    color={Color.WHITE}
                                                    small>
                                                    {errors.email?.message}
                                                </Label>
                                            )}
                                        </>
                                    )}
                                    name={'email'}
                                    defaultValue={''}
                                    rules={{
                                        required: Messages.Errors.emailBlank,
                                        pattern: {
                                            value: Constants.Regex.PASSWORD,
                                            message:
                                                Messages.Errors.emailValidity,
                                        },
                                    }}
                                />

                                <Controller
                                    control={control}
                                    render={({onChange, onBlur, value}) => (
                                        <>
                                            <TextInput
                                                placeholder={Strings.password}
                                                style={styles.inputFieldStyle}
                                                placeholderTextColor={
                                                    Color.WHITE
                                                }
                                                secureTextEntry
                                                value={value}
                                                onChangeText={onChange}
                                                onFocus={() =>
                                                    clearErrors('password')
                                                }
                                            />
                                            {errors.password?.message && (
                                                <Label
                                                    color={Color.WHITE}
                                                    small>
                                                    {errors.password?.message}
                                                </Label>
                                            )}
                                        </>
                                    )}
                                    name={'password'}
                                    defaultValue={''}
                                    rules={{required: Messages.Errors.pwdBlank}}
                                />
                                <Controller
                                    control={control}
                                    render={({onChange, onBlur, value}) => (
                                        <>
                                            <TextInput
                                                placeholder={
                                                    Strings.confirmPassword
                                                }
                                                style={styles.inputFieldStyle}
                                                placeholderTextColor={
                                                    Color.WHITE
                                                }
                                                secureTextEntry
                                                value={value}
                                                onChangeText={onChange}
                                                onFocus={() =>
                                                    clearErrors(
                                                        'confirmPassword',
                                                    )
                                                }
                                            />
                                            {errors.confirmPassword
                                                ?.message && (
                                                <Label
                                                    color={Color.WHITE}
                                                    small>
                                                    {
                                                        errors.confirmPassword
                                                            ?.message
                                                    }
                                                </Label>
                                            )}
                                        </>
                                    )}
                                    name={'confirmPassword'}
                                    defaultValue={''}
                                    rules={{required: Messages.Errors.pwdBlank}}
                                />
                            </View>
                            <View style={styles.vwLoginButton}>
                                <RoundButton
                                    click={handleSubmit(onSubmit)}
                                    width={ThemeUtils.relativeWidth(80)}
                                    mt={ThemeUtils.relativeRealHeight(4)}
                                    backgroundColor={Color.YELLOW01}
                                    textColor={Color.WHITE}
                                    visible={indicator}
                                    style={styles.submitLabelStyle}>
                                    {Strings.signUpCaps}
                                </RoundButton>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            )}
        </>
    );
};

export default SignUp;
