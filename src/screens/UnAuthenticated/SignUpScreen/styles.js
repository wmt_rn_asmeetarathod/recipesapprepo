import {StyleSheet} from 'react-native';
import {ThemeUtils} from 'src/utils';
import { Color } from '../../../utils';

export default StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center'

    },
    formContainer: {
        flex: 1,
        marginTop: ThemeUtils.relativeRealHeight(8),
        paddingHorizontal: ThemeUtils.relativeRealWidth(4),
        paddingVertical: ThemeUtils.relativeRealHeight(2),
        justifyContent: 'center',
        backgroundColor: "rgba(0,0,0,0.5)",
        padding: 30,
        margin: 30,
        // alignSelf : 'center'
    },
    logoStyle: {
        height: ThemeUtils.relativeWidth(25),
        width: ThemeUtils.relativeWidth(25),
        // marginTop: -20,
        marginBottom : 10,
        backgroundColor : Color.BLACK,
        borderRadius: ThemeUtils.relativeWidth(12.5),
        borderWidth :2,
        borderColor : 'white'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null

    },
    inputView: {
        width: '100%'
    },
    inputFieldStyle: {
        borderWidth: 1,
        borderColor: 'white',
        marginVertical: 10,
        color : Color.WHITE,
        fontSize : ThemeUtils.fontNormal,
        paddingStart : 10,

    },
    vwLoginButton:{
        alignItems : 'center',
    },
    submitLabelStyle:{
        flex :1,
        alignSelf : 'center',
    }
});

