import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 30,
        backgroundColor: Color.BLACK,
    },
    fullFlex: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        opacity: 0.9,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(80),
        width: ThemeUtils.relativeWidth(100),
        resizeMode: 'cover',
    },

    vwBottom: {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    vwArea: {
        flexDirection: 'row',
        marginVertical: 10,
    },
    vwInstructions: {},
    lblInstructions: {
        textAlign: 'justify',
    },
});
