import React, {useEffect, useState} from 'react';

import {View, Image, ImageBackground, ScrollView} from 'react-native';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import {styles} from './styles';
import {Label, NoInternetModal} from 'src/component';
import {APIRequest} from 'src/api';
import {Color, Strings, isNetworkConnected} from 'src/utils';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';

const FoodDetail = (props) => {
    const {itemId, fromApi} = props.route.params;
    const [data, setData] = useState();
    const [ingredients, setIngredients] = useState([]);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [indicatorVisible, setIndicatorVisible] = useState(false);

    /*  Life-cycles Methods */

    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    useEffect(() => {
        getData();
    }, []);

    const onPressTryAgain = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(true);
                getData();
            }
        });
    };

    const getData = () => {
        console.log('FoodDetails getData');
        if (fromApi) {
            getFoodDetail();
        } else {
            getFoodDetailFromDB();
        }
    };

    /*  UI Events Methods   */

    /* Fetch Food Details from itemId */
    const getFoodDetail = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${itemId}`,
            )
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();
    };

    const onResponse = (response) => {
        setData(response.data.meals[0]);
        getIngredients(response.data.meals[0]);
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(false);
                setNoInternetModalVisible(false);
            }
        });
    };

    const onError = (error) => {
        console.log('FoodDetail error ==>> ', error);
        if (error.status === 522) {
            console.log('error.status ==>>> ', error.status);
            setNoInternetModalVisible(true);
        }
    };

    const getFoodDetailFromDB = () => {
        database()
            .ref(`recipes`)
            .on('value', (snapshot) => {
                snapshot.forEach((data) => {
                    if (data.val()?.idMeal === itemId) {
                        database()
                            .ref(`recipes/${data.key}`)
                            .on('value', (snap) => {
                                setData(snap.val());
                                getIngredients(snap.val());
                            });
                    }
                });
            });

        isNetworkConnected().then((res) => {
            if (res && data !== null) {
                setIndicatorVisible(false);
                setNoInternetModalVisible(false);
            }
        });
    };
    /* To get ingredients Array */
    const getIngredients = (arr) => {
        let ingredientsArr = [];
        let tempArr = [];
        if (fromApi) {
            for (let index = 1; index <= 20; index++) {
                const element = arr[`strIngredient${index}`];
                if (element) {
                    ingredientsArr.push(element);
                }
            }
        } else {
            ingredientsArr = arr['strIngredient'];
        }
        ingredientsArr.forEach((item, index) => {
            if (index === ingredientsArr.length - 1) {
                item = item + '.';
                tempArr.push(item);
            } else {
                item = item + ', ';
                tempArr.push(item);
            }
        });
        setIngredients(tempArr);
    };

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}>
                    <ScrollView style={styles.fullFlex}>
                        <Image
                            source={{
                                uri: fromApi
                                    ? data && data.strMealThumb
                                    : data && data.recipePhoto,
                            }}
                            style={styles.foodImageStyle}
                        />
                        <View style={styles.container}>
                            <View style={styles.vwBottom}>
                                <View style={styles.vwArea}>
                                    <Label
                                        large
                                        bolder
                                        mt={10}
                                        mb={10}
                                        color={Color.WHITE}>
                                        {data && data.strArea}
                                    </Label>
                                </View>
                                <View>
                                    <Label bolder mb={10} color={Color.WHITE}>
                                        {Strings.ingredients}
                                    </Label>
                                    <Label
                                        color={Color.WHITE}
                                        mb={10}
                                        style={styles.lblInstructions}>
                                        {data &&
                                            ingredients.map((item) => {
                                                return item;
                                            })}{' '}
                                    </Label>
                                </View>
                                <View style={styles.vwInstructions}>
                                    <Label bolder mb={10} color={Color.WHITE}>
                                        {Strings.recipe}
                                    </Label>
                                    <Label
                                        color={Color.WHITE}
                                        style={styles.lblInstructions}>
                                        {data && data.strInstructions}
                                    </Label>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </ImageBackground>
            )}
        </>
    );
};

export default FoodDetail;
