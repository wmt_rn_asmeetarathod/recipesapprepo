import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    scrollViewStyle: {
        flex: 1,
        backgroundColor: Color.WHITE,
    },
    container: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        padding: 20,
        backgroundColor: Color.WHITE,
    },
    flatListStyle: {
        paddingTop: 30,
        paddingHorizontal: 30,
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        // opacity:0.7,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(30),
        width: ThemeUtils.relativeWidth(30),
        resizeMode: 'contain',
    },
    foodImageRadius: {
        borderRadius: ThemeUtils.relativeWidth(15),
    },
    editIconStyle: {
        position: 'absolute',
        bottom: 4,
        right: 4,
        backgroundColor: Color.CELLO,
        borderRadius: 50,
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(50),
        width: ThemeUtils.relativeWidth(40),
        justifyContent: 'space-between',
        marginHorizontal: 5,
        marginBottom: 20,
        borderRadius: 20,
        backgroundColor: Color.WHITE,
    },

    titleStyle: {
        backgroundColor: Color.CELLO,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 15,
    },
    vwUpper: {
        flexDirection: 'row',
        height: ThemeUtils.relativeHeight(18),
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    vwCounter: {
        flexDirection: 'row',
        backgroundColor: Color.WHITE,
        justifyContent: 'space-around',
        alignItems: 'center',
        // padding: 10,
        marginVertical: 20,
        elevation: 5,
    },
    vwNameTitle: {
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    vwToggleButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 5,
    },
    toggleButtonStyle: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    activeButtonStyle: {
        backgroundColor: Color.CELLO,
    },
    inActiveButtonStyle: {
        backgroundColor: Color.WHITE,
    },
    marginVertical: {
        marginVertical: 20,
    },
    vwCookBooksContent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    /* Header Style */
    headerStyle: {
        height: ThemeUtils.APPBAR_HEIGHT,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 0,
        backgroundColor: Color.WHITE,
        elevation: 0,
    },
    headerLeftIcon: {
        position: 'absolute',
        left: ThemeUtils.relativeWidth(2),
    },
    headerRightIcon: {
        position: 'absolute',
        right: ThemeUtils.relativeWidth(2),
        padding: 8,
    },
    headerRightAddIcon: {
        position: 'absolute',
        right: ThemeUtils.relativeWidth(12),
        padding: 8,
    },
    headerTitle: {
        textAlign: 'center',
        justifyContent: 'center',
        color: Color.CELLO,
        fontFamily: ThemeUtils.FontStyle.bold,
    },
    cookBookImageStyle: {
        flex: 1,
        width: undefined,
        height: undefined,
    },
    counterLabelStyle: {
        alignItems: 'center',
        padding: 10,
    },
    /* Modal Styles */
    modalContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        // flex: 1,
        width: ThemeUtils.relativeWidth(90),
    },
    vwUpperModal: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
    },
    modalView: {
        margin: 30,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 25,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        width: ThemeUtils.relativeWidth(90),
        // alignContent:'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    vwIndicator: {
        flex: 1,
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
});

