import React, {useEffect, useState} from 'react';
import {
    View,
    Image,
    Dimensions,
    ScrollView,
    Alert,
    Modal,
    ImageBackground,
    ActivityIndicator,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import ProgressCircle from 'react-native-progress-circle';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {styles} from './styles';
import {Label, Ripple, Hr, NoInternetModal} from 'src/component';
import {
    CommonStyle,
    Strings,
    Color,
    ThemeUtils,
    showToast,
    PermissionUtils,
    isNetworkConnected,
    // anonymousSignOut,
} from 'src/utils';
import Routes from 'src/router/Routes';
import FOOD_LOGO from 'src/assets/images/food_logo.png';
import USER_PLACEHOLDER from 'src/assets/images/user_placeholder_image.png';

const Profile = (props) => {
    const user = auth().currentUser;
    const [active, setActive] = useState('cookbook');
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const [userFullName, setUserFullName] = useState();
    const [userProfilePhoto, setUserProfilePhoto] = useState();
    const [following, setFollowing] = useState();
    const [followers, setFollowers] = useState();
    const [recipeData, setRecipeData] = useState([]);
    const [savedRecipeData, setSavedRecipeData] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [profileIndicator, setProfileIndicator] = useState(false);
    const [progress, setProgress] = useState(0);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [indicatorVisible, setIndicatorVisible] = useState(false);

    /*  Life-cycles Methods */
    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    useEffect(async () => {
        let mounted = true;
        if (mounted) {
            getData();
        }

        return () => (mounted = false);
    }, []);

    const onPressTryAgain = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(true);
                getData();
            }
        });
    };

    const getData = () => {
        database()
            .ref(`users/${user.uid}`)
            .on('value', (snapshot) => {
                setUserFullName(snapshot?.val()?.fullName);
                setUserProfilePhoto(snapshot?.val()?.photoURL);

                if (snapshot?.val()?.followingList) {
                    setFollowing(
                        Object.keys(snapshot?.val()?.followingList).length,
                    );
                } else {
                    setFollowing(0);
                }

                if (snapshot?.val()?.followerList) {
                    setFollowers(
                        Object.keys(snapshot?.val()?.followerList).length,
                    );
                } else {
                    setFollowers(0);
                }

                if (snapshot?.val()?.savedList) {
                    let savedList = snapshot?.val()?.savedList;
                    let savedRecipes = [];

                    for (const index in savedList) {
                        savedRecipes.push(savedList[index]);
                    }
                    setSavedRecipeData(savedRecipes);
                }
            });

        getRecipeData();
    };

    const getRecipeData = () => {
        database()
            .ref(`recipes`)
            .on('value', async (snapshot) => {
                let recipeArray = [];
                await Promise.all(
                    snapshot.forEach(async (data) => {
                        if (data.val()?.userUid === user.uid) {
                            await database()
                                .ref(`recipes/${data.key}`)
                                .on('value', (snap) => {
                                    recipeArray.push(snap?.val());
                                });
                        }
                    }),
                );
                setRecipeData(recipeArray);
            });
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(false);
                setNoInternetModalVisible(false);
            }
        });
    };

    const handleCookBook = (value) => {
        setActive(value);
    };

    const handleCollection = (value) => {
        setActive(value);
    };

    const images = [
        require('../../../assets/images/food1.jpg'),
        require('../../../assets/images/food2.jpg'),
        require('../../../assets/images/food3.jpg'),
        require('../../../assets/images/food4.jpg'),
        require('../../../assets/images/food5.jpg'),
        require('../../../assets/images/food6.jpg'),
        require('../../../assets/images/food7.jpg'),
        require('../../../assets/images/food8.jpg'),
        require('../../../assets/images/food9.jpg'),
        require('../../../assets/images/food10.jpg'),
        require('../../../assets/images/food11.jpg'),
        require('../../../assets/images/food12.jpg'),
        require('../../../assets/images/food13.jpeg'),
        require('../../../assets/images/food14.jpeg'),
        require('../../../assets/images/food15.jpeg'),
    ];

    /* Create Recipe */
    const handleAddRecipe = () => {
        props.navigation.navigate(Routes.CreateRecipe);
    };

    /* Handle User Log out */
    const handleLogOut = () => {
        Alert.alert(Strings.logOutTitle, Strings.sureWantToLogOut, [
            {
                text: Strings.cancel,
                onPress: () => null,
                style: 'cancel',
            },
            {
                text: Strings.logOutCaps,
                onPress: () => userSignOut(),
                style: 'cancel',
            },
        ]);
    };

    const userSignOut = async () => {
        auth()
            .signOut()
            .then(() => {
                props.navigation.navigate(Routes.UnAuthenticated);
                // anonymousSignOut();
                props.navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [
                            {
                                name: Routes.UnAuthenticated,
                                state: {
                                    routes: [
                                        {
                                            name: Routes.Login,
                                        },
                                    ],
                                },
                            },
                        ],
                    }),
                );
            });
    };

    const goToFoodDetail = (idMeal, strMeal) => {
        props.navigation.navigate(Routes.FoodDetail, {
            itemId: idMeal,
            itemName: strMeal,
            fromApi: false,
        });
    };

    const goToFollowersList = () => {
        props.navigation.navigate(Routes.UserList, {
            title: Strings.followers,
            userUid: user.uid,
        });
    };

    const goToFollowingList = () => {
        props.navigation.navigate(Routes.UserList, {
            title: Strings.following,
            userUid: user.uid,
        });
    };

    const handleEditPicture = () => {
        setModalVisible(true);
    };

    const uploadProgress = (ratio) => Math.round(ratio * 100);

    const storeImageToDB = (imageUrl) => {
        const userRef = storage().ref(`users/${user.uid}`);

        userRef.putFile(imageUrl.toString()).on('state_changed', (snapshot) => {
            const tempProgress = uploadProgress(
                snapshot.bytesTransferred / snapshot.totalBytes,
            );

            switch (snapshot.state) {
                case 'running':
                    setProgress(tempProgress);
                    break;
                case 'success':
                    setProgress(tempProgress);
                    userRef.getDownloadURL().then((url) => {
                        setUserProfilePhoto(url);
                        setProfileIndicator(false);
                        setProgress(0);
                        database()
                            .ref(`users/${user.uid}`)
                            .update({
                                photoURL: url,
                            })
                            .catch(() => {
                                setProfileIndicator(false);
                                setProgress(0);
                                showToast(Strings.cantUploadProfilePhoto);
                            });
                    });
                    break;

                default:
                    break;
            }
        });
    };

    const removeImageFromDB = async () => {
        setProgress(90);
        await storage()
            .ref(`users/${user.uid}`)
            .delete()
            .then(() => {
                setProgress(100);
                setProfileIndicator(false);
                setProgress(0);
                database()
                    .ref(`users/${user.uid}`)
                    .update({
                        photoURL: null,
                    })
                    .catch(() => {
                        setProfileIndicator(false);
                        setProgress(0);
                        showToast(Strings.cantUploadProfilePhoto);
                    });
            });
    };

    const handleCapturePhoto = () => {
        setModalVisible(false);

        PermissionUtils.requestCameraPermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openCamera();
            }
        });
    };

    const openCamera = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
            saveToPhotos: true,
        };
        launchCamera(options, (response) => {
            if (response.didCancel) {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else if (response.errorCode == 'camera_unavailable') {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else if (response.errorCode == 'permission') {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else if (response.errorCode == 'others') {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else {
                setProfileIndicator(true);
                storeImageToDB(response.assets[0].uri);
            }
        });
    };
    const handleChoosePhoto = () => {
        setModalVisible(false);

        /* For Storage Permission */
        PermissionUtils.requestStoragePermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openGallary();
            }
        });
    };

    const openGallary = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
        };
        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else if (response.errorCode == 'camera_unavailable') {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else if (response.errorCode == 'permission') {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else if (response.errorCode == 'others') {
                setUserProfilePhoto(imageUri);
                setProfileIndicator(false);
                setProgress(0);
            } else {
                setProfileIndicator(true);
                storeImageToDB(response.assets[0].uri);
            }
        });
    };
    const handleRemovePhoto = () => {
        setModalVisible(false);
        // setProfileIndicator(true);
        removeImageFromDB();
        setUserProfilePhoto('');
    };

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                <>
                    <View style={styles.headerStyle}>
                        <Ripple
                            style={styles.headerLeftIcon}
                            rippleContainerBorderRadius={50}
                            onPress={() => props.navigation.goBack()}>
                            <IonIcon
                                name={'arrow-back'}
                                size={27}
                                color={Color.CELLO}
                            />
                        </Ripple>

                        <Ripple
                            style={styles.headerRightAddIcon}
                            rippleContainerBorderRadius={70}
                            onPress={() => handleAddRecipe()}>
                            <IonIcon
                                name={'add-circle-outline'}
                                size={27}
                                color={Color.CELLO}
                            />
                        </Ripple>
                        <Ripple
                            style={styles.headerRightIcon}
                            rippleContainerBorderRadius={50}
                            onPress={() => handleLogOut()}>
                            <IonIcon
                                name={'log-out-outline'}
                                size={27}
                                color={Color.CELLO}
                            />
                        </Ripple>
                        <Label
                            style={styles.headerTitle}
                            color={Color.CELLO}
                            large>
                            {Strings.profile}{' '}
                        </Label>
                    </View>

                    {modalVisible && (
                        <View style={styles.modalContainer}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={modalVisible}
                                onRequestClose={() => setModalVisible(false)}>
                                <View style={styles.vwUpperModal}>
                                    <View style={styles.modalView}>
                                        <Label
                                            mb={10}
                                            onPress={handleCapturePhoto}>
                                            {Strings.capturePhoto}
                                        </Label>
                                        <Label
                                            mb={10}
                                            onPress={handleChoosePhoto}>
                                            {Strings.chooseFromGallary}
                                        </Label>
                                        {userProfilePhoto !== '' && (
                                            <Label onPress={handleRemovePhoto}>
                                                {Strings.removePhoto}
                                            </Label>
                                        )}
                                    </View>
                                </View>
                            </Modal>
                        </View>
                    )}
                    {profileIndicator ? (
                        <View style={styles.vwIndicator}>
                            <ProgressCircle
                                percent={progress}
                                radius={50}
                                borderWidth={8}
                                color={Color.CELLO}
                                shadowColor={Color.LIGHT_GRAY}
                                bgColor="#fff">
                                <Label>
                                    {progress}
                                    {'%'}
                                </Label>
                            </ProgressCircle>
                        </View>
                    ) : (
                        <ScrollView
                            style={styles.scrollViewStyle}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.container}>
                                <View style={styles.vwUpper}>
                                    <ImageBackground
                                        source={
                                            userProfilePhoto
                                                ? {uri: userProfilePhoto}
                                                : USER_PLACEHOLDER
                                        }
                                        style={styles.foodImageStyle}
                                        imageStyle={styles.foodImageRadius}>
                                        <Ripple
                                            rippleContainerBorderRadius={50}
                                            onPress={() => handleEditPicture()}
                                            style={styles.editIconStyle}>
                                            <IonIcon
                                                name="create-outline"
                                                style={styles.searchIcon}
                                                color={Color.WHITE}
                                                size={25}
                                            />
                                        </Ripple>
                                    </ImageBackground>
                                    <View style={styles.vwNameTitle}>
                                        <Label font_bold large>
                                            {userFullName && userFullName}
                                        </Label>
                                        <Label>{'Food Innovator'}</Label>
                                    </View>
                                </View>
                                <View style={styles.vwCounter}>
                                    <View style={styles.counterLabelStyle}>
                                        <Label font_bold large>
                                            {recipeData.length}
                                        </Label>
                                        <Label>{Strings.recipes}</Label>
                                    </View>
                                    <Ripple onPress={() => goToFollowingList()}>
                                        <View style={styles.counterLabelStyle}>
                                            <Label font_bold large>
                                                {following}
                                            </Label>
                                            <Label>{Strings.following}</Label>
                                        </View>
                                    </Ripple>
                                    <Ripple onPress={() => goToFollowersList()}>
                                        <View style={styles.counterLabelStyle}>
                                            <Label font_bold large>
                                                {followers}
                                            </Label>
                                            <Label>{Strings.followers}</Label>
                                        </View>
                                    </Ripple>
                                </View>
                                <View style={styles.vwToggleButton}>
                                    <Ripple
                                        style={[
                                            styles.toggleButtonStyle,
                                            active === 'cookbook'
                                                ? styles.activeButtonStyle
                                                : styles.inActiveButtonStyle,
                                        ]}
                                        onPress={() =>
                                            handleCookBook('cookbook')
                                        }>
                                        <Label
                                            color={
                                                active === 'cookbook'
                                                    ? Color.WHITE
                                                    : Color.DARK_LIGHT_BLACK
                                            }>
                                            {Strings.cookbooks}
                                        </Label>
                                    </Ripple>
                                    <Ripple
                                        style={[
                                            styles.toggleButtonStyle,
                                            active === 'collection'
                                                ? styles.activeButtonStyle
                                                : styles.inActiveButtonStyle,
                                        ]}
                                        onPress={() =>
                                            handleCollection('collection')
                                        }>
                                        <Label
                                            color={
                                                active === 'collection'
                                                    ? Color.WHITE
                                                    : Color.DARK_LIGHT_BLACK
                                            }>
                                            {Strings.collection}
                                        </Label>
                                    </Ripple>
                                </View>
                                <View style={styles.marginVertical}>
                                    <Hr />
                                </View>
                                {active === 'cookbook' && (
                                    <View style={styles.vwCookBooksContent}>
                                        {recipeData.map((recipe, index) => {
                                            return (
                                                <Ripple
                                                    key={index}
                                                    onPress={() =>
                                                        goToFoodDetail(
                                                            recipe.idMeal,
                                                            recipe.recipeName,
                                                        )
                                                    }>
                                                    <View
                                                        key={index}
                                                        style={[
                                                            {
                                                                width:
                                                                    width / 3 -
                                                                    15,
                                                                height:
                                                                    width / 3 -
                                                                    15,
                                                                marginBottom: 2,
                                                                paddingLeft:
                                                                    index %
                                                                        3 !==
                                                                    0
                                                                        ? 2
                                                                        : 0,
                                                            },
                                                        ]}>
                                                        <Image
                                                            style={
                                                                styles.cookBookImageStyle
                                                            }
                                                            source={{
                                                                uri: recipe.recipePhoto,
                                                            }}
                                                        />
                                                    </View>
                                                </Ripple>
                                            );
                                        })}
                                    </View>
                                )}
                                {active === 'collection' && (
                                    <View style={styles.vwCookBooksContent}>
                                        {savedRecipeData.map(
                                            (recipe, index) => {
                                                return (
                                                    <View
                                                        key={index}
                                                        style={[
                                                            {
                                                                width:
                                                                    width / 3 -
                                                                    15,
                                                                height:
                                                                    width / 3 -
                                                                    15,
                                                                marginBottom: 2,
                                                                paddingLeft:
                                                                    index %
                                                                        3 !==
                                                                    0
                                                                        ? 2
                                                                        : 0,
                                                            },
                                                        ]}>
                                                        <Image
                                                            style={
                                                                styles.cookBookImageStyle
                                                            }
                                                            source={{
                                                                uri: recipe.recipePhotoUrl,
                                                            }}
                                                        />
                                                    </View>
                                                );
                                            },
                                        )}
                                    </View>
                                )}
                            </View>
                        </ScrollView>
                    )}
                </>
            )}
        </>
    );
};

export default Profile;
