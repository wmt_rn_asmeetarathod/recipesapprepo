import React, {useEffect, useState} from 'react';
import {
    View,
    FlatList,
    Image,
    ImageBackground,
    Share,
    Modal,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import {styles} from './styles';

// custom component
import {Label, Ripple, NoInternetModal, Loader} from 'src/component';

// redux
import {connect} from 'react-redux';
import Routes from 'src/router/Routes';

// utils
import {Color, Strings, isNetworkConnected} from 'src/utils';

// third-party
import LinearGradient from 'react-native-linear-gradient';
import Voice from '@react-native-voice/voice';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';

// images
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';
import USER_PLACEHOLDER from 'src/assets/images/user_placeholder_image.png';

const FeedHome = (props) => {
    const user = auth().currentUser;
    const [data, setData] = useState([]);
    const [userFullName, setUserFullName] = useState();
    const [userProfilePhoto, setUserProfilePhoto] = useState();

    const [pitch, setPitch] = useState('');
    const [error, setError] = useState('');
    const [end, setEnd] = useState('');
    const [started, setStarted] = useState('');
    const [results, setResults] = useState([]);
    const [partialResults, setPartialResults] = useState([]);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [indicatorVisible, setIndicatorVisible] = useState(false);
    const [loading, setLoading] = useState(true);

    /*  Life-cycles Methods */

    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            getData();
        }

        return () => (mounted = false);
    }, []);

    const onPressTryAgain = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(true);
                getData();
            }
        });
    };

    const getData = () => {
        database()
            .ref(`recipes/`)
            .on('value', (snapshot) => {
                let recipe = [];
                snapshot.forEach((snap) => {
                    if (snap?.val()?.userUid !== user.uid) {
                        recipe.push(snap?.val());
                    }
                });
                setLoading(false);
                setData(recipe);
            });

        database()
            .ref(`users/${user.uid}`)
            .on('value', (snapshot) => {
                setUserFullName(snapshot?.val()?.fullName);
                setUserProfilePhoto(snapshot?.val()?.photoURL);
            });

        if (data.length > 0) {
            setIndicatorVisible(false);
            setNoInternetModalVisible(false);
        }
    };

    useEffect(() => {
        //Setting callbacks for the process status
        Voice.onSpeechStart = onSpeechStart;
        Voice.onSpeechRecognized = onSpeechRecognized;
        Voice.onSpeechEnd = onSpeechEnd;
        Voice.onSpeechError = onSpeechError;
        Voice.onSpeechResults = onSpeechResults;
        Voice.onSpeechPartialResults = onSpeechPartialResults;
        Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;

        return () => {
            //destroy the process after switching the screen
            Voice.destroy().then(Voice.removeAllListeners);
        };
    }, []);

    const onSpeechStart = (e) => {
        setStarted('√');
    };

    const onSpeechRecognized = (e) => {
        console.log('onSpeechRecognized -->>> ', e);
    };

    const onSpeechEnd = (e) => {
        setEnd('√');
    };

    const onSpeechError = (e) => {
        setError(JSON.stringify(e.error));
    };

    const onSpeechPartialResults = (e) => {
        setPartialResults(e.value);
    };

    const onSpeechResults = (e) => {
        setResults(e.value);
    };

    const onSpeechVolumeChanged = (e) => {
        setPitch(e.value);
    };

    const startRecognition = async () => {
        try {
            await Voice.start('en-US');
            setPitch('');
            setError('');
            setStarted('');
            setResults([]);
            setPartialResults([]);
            setEnd('');
        } catch (error) {
            console.log('startRecognition error ==>> ', error);
        }
    };

    //Stops listening for speech
    const stopRecognizing = async () => {
        try {
            await Voice.stop();
        } catch (e) {
            console.error(e);
        }
    };

    /*  UI Events Methods   */

    const openProfile = (id) => {
        props.navigation.navigate(Routes.UserDetails, {otherUserUid: id});
    };

    const onPressShare = async (recipePhoto) => {
        try {
            const result = await Share.share({
                message: 'Recipes App | To guide how to cook',
                title: 'RECIPES',
                url: recipePhoto,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log(
                        'IF result.activityType ==>> ',
                        result.activityType,
                    );
                } else {
                    console.log(
                        'ELSE result.activityType ==>> ',
                        result.activityType,
                    );
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('Dismissed!!');
            }
        } catch (error) {
            console.log('Error -->> ', error);
        }
    };

    const goToLikedUsersList = (idMeal) => {
        props.navigation.navigate(Routes.UserList, {
            title: Strings.likes,
            idMeal: idMeal,
        });
    };

    /*  Custom-Component sub-render Methods */

    // To render comment
    const RenderComment = ({itemData}) => {
        const {content, userProfileUrl} = itemData;

        return (
            <>
                <View style={styles.vwRenderComment}>
                    <Image
                        source={{uri: userProfileUrl}}
                        style={styles.userPhotoInCommentStyle}
                    />
                    <Label small color={Color.WHITE} ms={10}>
                        {content}
                    </Label>
                </View>
            </>
        );
    };

    // Component for Empty list of Comment
    const RenderCommentListEmptyCompo = () => {
        return (
            <View style={styles.container}>
                <Label color={Color.WHITE}> {Strings.noCommentsYet}</Label>
            </View>
        );
    };

    const RenderItemCompo = ({itemData}) => {
        const {userPhotoUrl, recipePhoto, userName, userUid, idMeal} = itemData;
        const [liked, setLiked] = useState(false);
        const [saved, setSaved] = useState(false);
        const [likeCounter, setLikeCounter] = useState(
            itemData?.likes ? Object.keys(itemData?.likes)?.length : 0,
        );
        const [modalVisible, setModalVisible] = useState(false);
        const [commentInput, setCommentInput] = useState('');
        const [commentList, setCommentList] = useState([]);

        /*  Life-cycles Methods */
        useEffect(() => {
            let mounted = true;
            if (mounted) {
                database()
                    .ref(`recipes`)
                    .once('value')
                    .then((snapshot) => {
                        snapshot.forEach((data) => {
                            if (data.val()?.idMeal === idMeal) {
                                if (data.val()?.likes) {
                                    if (
                                        data
                                            .val()
                                            .likes.hasOwnProperty(user.uid)
                                    ) {
                                        setLiked(true);
                                    }
                                }
                                if (data.val()?.comments) {
                                    let commentsArray = [];
                                    let list = data.val()?.comments;

                                    for (const index in list) {
                                        commentsArray.push(list[index]);
                                    }
                                    setCommentList(commentsArray);
                                }
                            }
                        });
                    })
                    .catch((error) => {
                        console.log(
                            'FeedHome useEffect recipes error ==>> ',
                            error,
                        );
                    });

                database()
                    .ref(`users/${user.uid}/`)
                    .on('value', (snapshot) => {
                        if (snapshot?.val()?.savedList) {
                            if (
                                snapshot
                                    ?.val()
                                    ?.savedList.hasOwnProperty(idMeal)
                            ) {
                                setSaved(true);
                            }
                        }
                    });
            }
            return () => (mounted = false);
        }, []);

        /*  UI Events Methods   */

        const onPressSave = () => {
            setSaved(!saved);
            if (!saved) {
                database().ref(`users/${user.uid}/savedList/${idMeal}`).set({
                    recipePhotoUrl: recipePhoto,
                    userUid: userUid,
                    recipeId: idMeal,
                    userFullName: userName,
                });
            } else {
                database()
                    .ref(`users/${user.uid}/savedList/${idMeal}`)
                    .remove();
            }
        };

        const handleOnPressLike = () => {
            setLiked(!liked);
            if (!liked) {
                database()
                    .ref(`recipes`)
                    .once('value')
                    .then((snapshot) => {
                        snapshot.forEach((data) => {
                            if (data.val()?.idMeal === idMeal) {
                                database()
                                    .ref(
                                        `recipes/${data.key}/likes/${user.uid}`,
                                    )
                                    .set({
                                        userUid: user.uid,
                                        userFullName: userFullName,
                                        userProfilePhotoUrl: userProfilePhoto,
                                    });
                            }
                        });
                    });
            } else {
                database()
                    .ref(`recipes`)
                    .once('value')
                    .then((snapshot) => {
                        snapshot.forEach((data) => {
                            if (data.val()?.idMeal === idMeal) {
                                database()
                                    .ref(
                                        `recipes/${data.key}/likes/${user.uid}`,
                                    )
                                    .remove();
                            }
                        });
                    });
            }
        };

        const onPressComment = () => {
            setModalVisible(true);
        };

        const handleOnSendComment = () => {
            if (commentInput.length !== 0) {
                database()
                    .ref(`recipes`)
                    .once('value')
                    .then((snapshot) => {
                        snapshot.forEach((data) => {
                            if (data.val()?.idMeal === idMeal) {
                                database()
                                    .ref(
                                        `recipes/${data.key}/comments/${user.uid}`,
                                    )
                                    .set({
                                        content: commentInput,
                                        userUid: user.uid,
                                        userFullName: userFullName,
                                        userProfileUrl: userProfilePhoto,
                                        createdAt: new Date(),
                                    })
                                    .then(() => {
                                        setCommentInput('');
                                    });
                            }
                        });
                    });
            }
        };

        const renderModal = () => (
            <View style={styles.modalContainer}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => setModalVisible(false)}>
                    <View style={styles.vwUpperModal}>
                        <View style={styles.modalView}>
                            <LinearGradient
                                colors={[
                                    'transparent',
                                    'rgba(0,0,0,0.7)',
                                    'rgba(0,0,0,0.9)',
                                ]}>
                                <View style={styles.gradientView}>
                                    <Ripple
                                        rippleContainerBorderRadius={50}
                                        onPress={() => setModalVisible(false)}>
                                        <IonIcon
                                            name={'close-circle-outline'}
                                            size={27}
                                            color={Color.WHITE}
                                        />
                                    </Ripple>
                                </View>
                            </LinearGradient>
                            <View style={styles.solidView}>
                                <FlatList
                                    style={styles.flatListStyle}
                                    contentContainerStyle={{
                                        paddingBottom: 50,
                                    }}
                                    data={commentList}
                                    renderItem={({item}) => (
                                        <RenderComment itemData={item} />
                                    )}
                                    keyExtractor={(item) =>
                                        item.createdAt.toString()
                                    }
                                    showsVerticalScrollIndicator={false}
                                    ListEmptyComponent={
                                        <RenderCommentListEmptyCompo />
                                    }
                                />

                                <View style={styles.vwInputComment}>
                                    <TextInput
                                        placeholder="Leave a Comment"
                                        style={styles.inputFieldCommentStyle}
                                        placeholderTextColor={Color.GRAY01}
                                        value={commentInput}
                                        onChangeText={(text) =>
                                            setCommentInput(text)
                                        }
                                    />
                                    <Ripple
                                        style={styles.padding}
                                        rippleContainerBorderRadius={50}
                                        onPress={() => {
                                            commentInput?.length === 0
                                                ? null
                                                : handleOnSendComment();
                                        }}>
                                        <IonIcon
                                            name={'send-sharp'}
                                            size={27}
                                            color={
                                                commentInput.length === 0
                                                    ? Color.GRAY01
                                                    : Color.WHITE
                                            }
                                        />
                                    </Ripple>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );

        return (
            <>
                {renderModal()}
                <View style={styles.itemStyle}>
                    <View
                        style={styles.vwHeader}
                        onStartShouldSetResponder={() => openProfile(userUid)}>
                        <Image
                            source={
                                userPhotoUrl
                                    ? {uri: userPhotoUrl}
                                    : USER_PLACEHOLDER
                            }
                            style={styles.userPhotoStyle}
                        />
                        <Label style={styles.titleStyle} color={Color.CELLO}>
                            {userName}
                        </Label>
                    </View>
                    <Image
                        source={{uri: recipePhoto}}
                        style={styles.foodImageStyle}
                    />
                    <View style={styles.vwFooter}>
                        <View style={styles.vwUpper}>
                            <View style={styles.vwLeft}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={styles.marginIcon}
                                    onPress={() => handleOnPressLike()}>
                                    <IonIcon
                                        name={liked ? 'heart' : 'heart-outline'}
                                        size={30}
                                        color={liked ? Color.RED : Color.CELLO}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={styles.marginIcon}
                                    onPress={() => onPressComment()}>
                                    <IonIcon
                                        name={'chatbubble-outline'}
                                        size={27}
                                        color={Color.CELLO}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={styles.sendIcon}
                                    onPress={() => onPressShare(recipePhoto)}>
                                    <IonIcon
                                        name={'send-sharp'}
                                        size={27}
                                        style={styles.rotateSendIcon}
                                        color={Color.CELLO}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.vwRight}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    onPress={() => onPressSave()}>
                                    <IonIcon
                                        name={
                                            saved
                                                ? 'bookmark'
                                                : 'bookmark-outline'
                                        }
                                        size={27}
                                        color={Color.CELLO}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.vwLikes}>
                            <Label
                                font_bold
                                small
                                onPress={() => {
                                    goToLikedUsersList(idMeal);
                                }}>{`${likeCounter} ${
                                likeCounter === 1 ? Strings.like : Strings.likes
                            }`}</Label>
                        </View>
                    </View>
                </View>
            </>
        );
    };

    return (
        <>
            <Loader loading={loading} />
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                /*   <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}> */
                <View style={styles.container}>
                    <FlatList
                        style={styles.flatListStyle}
                        contentContainerStyle={styles.contentContainerStyle}
                        data={data}
                        renderItem={({item}) => (
                            <RenderItemCompo itemData={item} />
                        )}
                        keyExtractor={(item) => item.idMeal.toString()}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                // </ImageBackground>
            )}
        </>
    );
};

const mapStateToProps = (state) => {
    return {networkStatus: state.networkStatus};
};

export default connect(mapStateToProps)(FeedHome);
