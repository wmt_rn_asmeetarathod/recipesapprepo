import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flatListStyle: {
        // paddingTop: 30,
        // paddingHorizontal: 10,
        flex: 1,
    },
    contentContainerStyle: {
        paddingBottom: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.WHITE,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
    },
    userPhotoStyle: {
        height: ThemeUtils.relativeWidth(10),
        width: ThemeUtils.relativeWidth(10),
        borderRadius: ThemeUtils.relativeWidth(5),
    },
    userPhotoInCommentStyle: {
        height: ThemeUtils.relativeWidth(8),
        width: ThemeUtils.relativeWidth(8),
        borderRadius: ThemeUtils.relativeWidth(4),
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(50),
        width: ThemeUtils.relativeWidth(100),
        alignSelf: 'center',
        resizeMode: 'cover',
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(80),
        width: ThemeUtils.relativeWidth(100),
        marginVertical: 10,
        alignSelf: 'center',
        backgroundColor: Color.WHITE,
        justifyContent: 'space-around',
        // padding: 10,
    },
    vwHeader: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 10,
        height: ThemeUtils.relativeHeight(7),
    },
    titleStyle: {
        paddingHorizontal: 10,
    },
    vwFooter: {
        paddingHorizontal: 10,
        justifyContent: 'center',
        height: ThemeUtils.relativeHeight(9),
    },
    vwUpper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        marginTop: 2.5,
        paddingBottom: 5,
        height: ThemeUtils.relativeHeight(6.5),
    },
    vwLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    vwRight: {},
    marginIcon: {
        marginEnd: 7,
        paddingBottom: 1,
    },
    sendIcon: {
        marginHorizontal: 3,
        position: 'absolute',
        left: 75,
        bottom: 3,
    },
    rotateSendIcon: {
        transform: [
            {
                rotateZ: '330deg',
            },
        ],
    },
    vwLikes: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        height: ThemeUtils.relativeHeight(2),
        paddingStart: 4,
    },
    vwComment: {
        backgroundColor: '#80000000',
        height: ThemeUtils.relativeHeight(50),
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    modalContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        width: ThemeUtils.relativeWidth(90),
    },
    vwUpperModal: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignContent: 'center',
        flex: 1,
    },
    modalView: {
        padding: 25,
        alignItems: 'center',
        justifyContent: 'center',

        alignSelf: 'center',
        height: ThemeUtils.relativeHeight(50),
        width: ThemeUtils.relativeWidth(100),
        alignContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    gradientView: {
        height: ThemeUtils.relativeHeight(10),
        width: ThemeUtils.relativeWidth(100),
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 10,
    },
    solidView: {
        height: ThemeUtils.relativeHeight(40),
        width: ThemeUtils.relativeWidth(100),
        backgroundColor: 'rgba(0,0,0,0.9)',
    },
    vwInputComment: {
        position: 'absolute',
        bottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: ThemeUtils.relativeWidth(100),
        backgroundColor: Color.BLACK,
    },
    inputFieldCommentStyle: {
        width: ThemeUtils.relativeWidth(80),
        borderRadius: 8,
        padding: 10,
        backgroundColor: Color.GRAY02,
        color: Color.WHITE,
        fontFamily: ThemeUtils.FontStyle.regular,
    },
    vwRenderComment: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    padding: {
        padding: 10,
    },
});
