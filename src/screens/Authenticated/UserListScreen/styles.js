import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    flatListStyle: {
        paddingTop: 30,
        paddingHorizontal: 25,
        flex: 1
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        opacity: 0.9

    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(10),
        width: ThemeUtils.relativeWidth(10),
        borderRadius: ThemeUtils.relativeWidth(5),
        alignSelf: 'flex-start'
    },
    itemStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        margin: 5,
        borderRadius: 15,
        backgroundColor: Color.WHITE,
        padding: 10
    },
    titleStyle: {
        paddingStart: 20,
        flexWrap: 'wrap',
        width: ThemeUtils.relativeWidth(40)
    },
    emptyContainerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems : 'center',
    },
});

