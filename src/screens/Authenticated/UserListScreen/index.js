import React, {useEffect, useState} from 'react';
import {
    View,
    FlatList,
    Image,
    ImageBackground,
    Share,
    Modal,
    TextInput,
    TouchableOpacity
} from 'react-native';
import Routes from 'src/router/Routes';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {styles} from './styles';
import {Label, Ripple, NoInternetModal} from 'src/component';
import {Color, Strings, isNetworkConnected} from 'src/utils';
import LinearGradient from 'react-native-linear-gradient';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';

const UserList = (props) => {
    const {userUid, title} = props.route.params;
    const user = auth().currentUser;
    const [data, setData] = useState([]);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [indicatorVisible, setIndicatorVisible] = useState(false);

      /*  Life-cycles Methods */
      useEffect(() => {
          isNetworkConnected().then((res) => {
              if (!res) {
                  setNoInternetModalVisible(true);
              }
          });
      }, []);

    const onPressTryAgain = () => {
        
          isNetworkConnected().then((res) => {
              if (res) {
                  setIndicatorVisible(true);
                  if (data.length > 0)
                  {
                      setIndicatorVisible(false);
                      setNoInternetModalVisible(false);
                  }
                  
              }
          });
    };
    

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            if (title === Strings.followers) {
                getFollowerList();
            }
            if (title === Strings.following) {
                getFollowingList();
            }
            if (title === Strings.likes) {
                getLikedUserList();
            }

        }
        return() => {
            mounted = false;
        }
    }, []);

    const getFollowerList = () => {
        database().ref(`users/${
            userUid
        }/followerList`).on('value', snapshot => {
            let userArray = [];
            let list = snapshot?.val();
            for (const index in list) {
                userArray.push(list[index]);
            }
            setData(userArray);

        })
    }

    const getFollowingList = () => {
        database().ref(`users/${
            userUid
        }/followingList`).on('value', snapshot => {
            let userArray = [];
            let list = snapshot?.val();
            for (const index in list) {
                userArray.push(list[index]);
            }
            setData(userArray);
        })
    }

    const getLikedUserList = () => {
        const { idMeal } = props.route.params;
        database().ref(`recipes`).once('value').then(snapshot => {

            snapshot.forEach((data) => {

                if (data.val()?.idMeal === idMeal) {

                    database().ref(`recipes/${
                        data.key
                    }/likes/`).on('value',snapshot=>{
                        let userArray = [];
                        let list = snapshot?.val();
                        for (const index in list) {
                            userArray.push(list[index]);
                        }
                        setData(userArray);
                    })
                }
            })

        })
    }

    const openProfile = (id) => {
        props.navigation.navigate(Routes.UserDetails, {otherUserUid: id});
    }

    const RenderListEmptyCompo = () => {
        return (<View style={
            styles.emptyContainerStyle
        }>
            <Label font_bold
                color={
                    Color.WHITE
            }> {
                Strings.noUsersFound
            }</Label>
        </View>)
    }

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const {userFullName, userProfilePhotoUrl, userUid} = itemData;

        return (<View>
            <Ripple rippleContainerBorderRadius={0}
                style={
                    styles.itemStyle
                }
                onPress={
                    () => openProfile(userUid)
            }>
                <Image source={
                        {uri: userProfilePhotoUrl}
                    }
                    style={
                        styles.foodImageStyle
                    }/>
                <Label style={
                        styles.titleStyle
                    }
                    color={
                        Color.CELLO
                }> {userFullName}</Label>

            </Ripple>
        </View>);
    }

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}>
                    <View style={styles.container}>
                        <FlatList
                            style={styles.flatListStyle}
                            contentContainerStyle={styles.contentContainerStyle}
                            data={data}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.userUid.toString()}
                            ListEmptyComponent={<RenderListEmptyCompo />}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </ImageBackground>
            )}
        </>
    );
}

export default UserList;

