import React, {
    useEffect, 
    useState
} from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {
    View, 
    ImageBackground, 
    ScrollView, 
    ToastAndroid,
    Dimensions,
    Image,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import {styles} from './styles';
import {
    MaterialTextInput,
    RoundButton,
    Label,
    Ripple,
    NoInternetModal,
} from 'src/component';
import {
    CommonStyle,
    Color,
    ThemeUtils,
    Strings,
    isNetworkConnected,
} from 'src/utils';
import {APIRequest} from 'src/api';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';
import ImagePicker from 'react-native-image-crop-picker';

const CreateRecipe = (props) => {
    const user = auth().currentUser;
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [categories, setCategories] = useState([]);
    const [areas, setAreas] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [openArea, setOpenArea] = useState(false);
    const [openCategory, setOpenCategory] = useState(false);
    const [openIngredients, setOpenIngredients] = useState(false);

    const [selectedArea, setSelectedArea] = useState();
    const [selectedCategory, setSelectedCategory] = useState();
    const [selectedIngredients, setSelectedIngredients] = useState([
        'Bacon',
        'Onion',
        'Tomato',
        'Paprika',
        'Vegetable Oil',
        'Banana',
        'Garlic',
        'Sour Cream',
        'Mozzarella',
        'Butter',
    ]);
    const recipeId = Math.floor(Math.random() * 100000000000000000) + 1;
    const [recipeInstructions, setRecipeInstructions] = useState();
    const [recipeName, setRecipeName] = useState();
    const [recipePhoto, setRecipePhoto] = useState(
        'https://media.istockphoto.com/photos/idly-sambar-or-idli-with-sambhar-and-green-red-chutney-popular-south-picture-id1024549286?k=6&m=1024549286&s=612x612&w=0&h=ru9RBzXUNeD_a38mZbvH2J29seGc--fZcWotFr0gJVQ=',
    );
    const [userFullName, setUserFullName] = useState();
    const [recipePhotosUrls, setRecipePhotosUrls] = useState([]);
    const [userProfilePhoto, setUserProfilePhoto] = useState();
    const [imageArray, setImageArray] = useState([]);

    /*  Life-cycles Methods */
    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    const checkNetwork = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setNoInternetModalVisible(false);
            }
        });
    };

    
    useEffect(() => {
        areas.length === 0 && getAreaList();
        categories.length === 0 && getCategoryList();
        ingredients.length === 0 && getIngredientsList();
        database()
            .ref(`users/${user.uid}`)
            .on('value', (snapshot) => {
                setUserFullName(snapshot.val().fullName);
                setUserProfilePhoto(snapshot.val().photoURL);
            });
    }, []);

    const getAreaList = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(`https://www.themealdb.com/api/json/v1/1/list.php?a=list`)
            .response(onAreaResponse)
            .error(onAreaError)
            .build()
            .doRequest();
    };

    const onAreaResponse = (response) => {
        let areasArr = [];
        areasArr = response.data.meals;
        areasArr.map(({strArea}) => {
            areas.push({label: strArea, value: strArea});
        });
        setAreas(areas);
    };
    const onAreaError = (error) => {
        console.log('Area error ==>> ', error);
    };

    const getCategoryList = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(`https://www.themealdb.com/api/json/v1/1/list.php?c=list`)
            .response(onCategoryResponse)
            .error(onCategoryError)
            .build()
            .doRequest();
    };

    const onCategoryResponse = (response) => {
        let categoriesArr = [];
        categoriesArr = response.data.meals;
        categoriesArr.map(({strCategory}) => {
            categories.push({label: strCategory, value: strCategory});
        });
        setCategories(categories);
    };
    const onCategoryError = (error) => {
        console.log('Category error ==>> ', error);
    };

    const getIngredientsList = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(`https://www.themealdb.com/api/json/v1/1/list.php?i=list`)
            .response(onIngredientsResponse)
            .error(onIngredientsError)
            .build()
            .doRequest();
    };

    const onIngredientsResponse = (response) => {
        let ingredientsArr = [];
        ingredientsArr = response.data.meals;
        ingredientsArr.map(({strIngredient}) => {
            ingredients.push({label: strIngredient, value: strIngredient});
        });
        setIngredients(ingredients);
    };

    const onIngredientsError = (error) => {
        console.log('Ingredient error ==>> ', error);
    };

    const onOpenArea = () => {
        setOpenCategory(false);
        setOpenIngredients(false);
    };

    const onOpenCategory = () => {
        setOpenArea(false);
        setOpenIngredients(false);
    };
    const onOpenIngredients = () => {
        setOpenArea(false);
        setOpenCategory(false);
    };

    const uploadImagesToStorage = async (arr) => {
        let photosArr = [];

        return new Promise(async (resolve, reject) => {
            await Promise.all(
                arr.map(async (image, index) => {
                    await storage()
                        .ref(`${recipeId}/${index}`)
                        .putFile(image.path)
                        .then(() => {
                            console.log('Image Uploaded');
                            storage()
                                .ref(`${recipeId}/${index}`)
                                .getDownloadURL()
                                .then((url) => {
                                    photosArr.push(url);
                                });
                        });
                }),
            );
            setRecipePhotosUrls(photosArr);

            photosArr.length > 0
                ? resolve(photosArr)
                : reject('error while Upload Image');
        });
    };

    const onPressCreate = async () => {
        isNetworkConnected().then(async(res) => {
             if (res) {
                 setNoInternetModalVisible(false);
                 await uploadImagesToStorage(imageArray)
                     .then((arr) => {
                         database()
                             .ref(`recipes/`)
                             .push()
                             .set({
                                 idMeal: recipeId,
                                 userName: userFullName,
                                 recipeName: recipeName,
                                 strArea: selectedArea,
                                 strCategory: selectedCategory,
                                 strIngredient: selectedIngredients,
                                 recipePhoto: arr[0],
                                 recipePhotosArray: arr,
                                 strInstructions: recipeInstructions,
                                 userPhotoUrl: userProfilePhoto,
                                 userUid: user.uid,
                                 createdAt: new Date(),
                             })
                             .then(() => {
                                 ToastAndroid.show(
                                     Strings.recipeCreatedSuccessfully,
                                     ToastAndroid.LONG,
                                 );
                                 props.navigation.pop();
                             });
                     })
                     .catch((error) => {
                         console.log('onPressCreate error ==>> ', error);
                         if (error.code === 'auth/network-request-failed') {
                             setNoInternetModalVisible(true);
                         }
                     });
             } else {
                 setNoInternetModalVisible(true);
             }
        });
        
    };

    const openImagePicker = () => {
        ImagePicker.openPicker({
            multiple: true,
            maxFiles: 5,
        }).then((image) => {
            if (image.length > 5) {
                image.splice(-(image.length - 5), image.length - 5);
            }
            setImageArray(image);
        });
    };

    const RenderPhotoBox = () => {
        return (
            <Ripple>
                <View
                    style={[
                        styles.vwInputPhotos,
                        {
                            width: width / 5 - 20,
                            height: width / 5 - 20,
                            margin: 4,
                        },
                    ]}>
                    <IonIcon name={'add'} size={35} color={Color.WHITE} />
                </View>
            </Ripple>
        );
    };

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={checkNetwork}
                />
            ) : (
                <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}>
                    <ScrollView style={CommonStyle.full_flex}>
                        <View style={styles.container}>
                            <View>
                                <Ripple onPress={openImagePicker}>
                                    <View style={styles.vwUploadPhotosLabel}>
                                        <Label color={Color.DARK_LIGHT_BLACK}>
                                            {Strings.uploadPhotos}
                                        </Label>
                                    </View>
                                </Ripple>

                                <View style={styles.vwCookBooksContent}>
                                    {imageArray.length > 0 &&
                                        imageArray.map((imageData, index) => {
                                            return (
                                                <Ripple
                                                    key={index}
                                                    style={
                                                        styles.rplImagesStyle
                                                    }>
                                                    <View
                                                        key={index}
                                                        style={[
                                                            {
                                                                width:
                                                                    width / 5 -
                                                                    15,
                                                                height:
                                                                    width / 5 -
                                                                    15,
                                                                margin: 2,
                                                            },
                                                        ]}>
                                                        <Image
                                                            style={
                                                                styles.cookBookImageStyle
                                                            }
                                                            source={{
                                                                uri: imageData.path,
                                                            }}
                                                        />
                                                    </View>
                                                </Ripple>
                                            );
                                        })}
                                    {Array.from(
                                        {length: 5 - imageArray.length},
                                        (_, index) => (
                                            <RenderPhotoBox key={index} />
                                        ),
                                    )}
                                </View>

                                <MaterialTextInput
                                    style={styles.recipeNameStyle}
                                    placeholder={Strings.enterRecipeName}
                                    lineWidth={0}
                                    activeLineWidth={0}
                                    disabledLineWidth={0}
                                    placeholderTextColor={
                                        Color.DARK_LIGHT_BLACK
                                    }
                                    value={recipeName}
                                    onChangeText={(text) => setRecipeName(text)}
                                />
                                <DropDownPicker
                                    listMode="SCROLLVIEW"
                                    open={openArea}
                                    value={selectedArea}
                                    items={areas}
                                    setOpen={setOpenArea}
                                    onOpen={onOpenArea}
                                    setValue={setSelectedArea}
                                    setItems={setAreas}
                                    placeholder="Select Area"
                                    disableBorderRadius={true}
                                    onChangeValue={(value) => {
                                        setSelectedArea(value);
                                    }}
                                    style={styles.dropDownStyle}
                                    placeholderStyle={
                                        styles.dropDownPlaceHolderStyle
                                    }
                                    dropDownContainerStyle={
                                        styles.dropDownContainerStyle
                                    }
                                    closeAfterSelecting={true}
                                    listItemLabelStyle={
                                        styles.listItemLabelStyle
                                    }
                                />

                                <DropDownPicker
                                    listMode="SCROLLVIEW"
                                    open={openCategory}
                                    value={selectedCategory}
                                    items={categories}
                                    setOpen={setOpenCategory}
                                    onOpen={onOpenCategory}
                                    setValue={setSelectedCategory}
                                    setItems={setCategories}
                                    placeholder="Select Category"
                                    disableBorderRadius={true}
                                    onChangeValue={(value) => {
                                        setSelectedCategory(value);
                                    }}
                                    style={styles.dropDownStyle}
                                    placeholderStyle={
                                        styles.dropDownPlaceHolderStyle
                                    }
                                    dropDownContainerStyle={
                                        styles.dropDownContainerStyle
                                    }
                                    closeAfterSelecting={true}
                                    listItemLabelStyle={
                                        styles.listItemLabelStyle
                                    }
                                />
                                <MaterialTextInput
                                    style={styles.inputStyle}
                                    placeholder={Strings.enterRecipeHere}
                                    multiline
                                    lineWidth={0}
                                    numberOfLines={8}
                                    activeLineWidth={0}
                                    disabledLineWidth={0}
                                    placeholderTextColor={
                                        Color.DARK_LIGHT_BLACK
                                    }
                                    value={recipeInstructions}
                                    onChangeText={(text) =>
                                        setRecipeInstructions(text)
                                    }
                                />
                            </View>
                            <View style={styles.vwButton}>
                                <RoundButton
                                    click={onPressCreate}
                                    width={ThemeUtils.relativeWidth(80)}
                                    mt={ThemeUtils.relativeRealHeight(4)}
                                    backgroundColor={Color.WHITE}
                                    textColor={Color.CELLO}
                                    style={styles.createButtonStyle}>
                                    {Strings.create}{' '}
                                </RoundButton>
                            </View>
                        </View>
                    </ScrollView>
                </ImageBackground>
            )}
        </>
    );
}

export default CreateRecipe;

