import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        padding: 20,
        flexDirection: 'column',
        height: ThemeUtils.relativeHeight(90),
        justifyContent: 'space-between',
    },
    flatListStyle: {
        paddingTop: 30,
        paddingHorizontal: 30,
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        // opacity:0.9,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(30),
        width: ThemeUtils.relativeWidth(40),
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexGrow: 1,
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(50),
        width: ThemeUtils.relativeWidth(40),
        justifyContent: 'space-between',
        marginHorizontal: 5,
        marginBottom: 20,
        borderRadius: 20,
        backgroundColor: Color.WHITE,
    },

    titleStyle: {
        backgroundColor: Color.CELLO,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 15,
    },
    vwInput: {
        height: ThemeUtils.relativeHeight(25),
        backgroundColor: 'red',
        borderWidth: 1,
        borderColor: Color.WHITE,
        padding: 10,
        alignContent: 'flex-start',
    },
    inputStyle: {
        height: ThemeUtils.relativeHeight(25),
        maxHeight: ThemeUtils.relativeHeight(25),
        backgroundColor: Color.WHITE,
        width: ThemeUtils.relativeWidth(90),
        position: 'absolute',
        top: -25,
        paddingHorizontal: 10,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'justify',
        borderWidth: 1,
        borderColor: Color.WHITE,
        opacity: 0.7,
        
    },
    recipeNameStyle: {
        textAlign: 'left',
        paddingHorizontal: 10,
        backgroundColor: Color.WHITE,
        opacity: 0.7,
        height: ThemeUtils.relativeHeight(5),
        alignItems: 'center',
        textAlignVertical: 'center',
    },
    vwButton: {
        alignSelf: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 5,
    },
    dropDownStyle: {
        borderRadius: 0,
        borderWidth: 0,
        color: Color.BLACK,
        fontFamily: ThemeUtils.FontStyle.regular,
        alignSelf: 'center',
        marginVertical: 10,
    },
    dropDownPlaceHolderStyle: {
        color: Color.DARK_LIGHT_BLACK,
        fontFamily: ThemeUtils.FontStyle.regular,
    },
    dropDownContainerStyle: {
        backgroundColor: Color.WHITE,
        borderRadius: 0,
        borderWidth: 0,
        fontFamily: ThemeUtils.FontStyle.regular,
        elevation: 2,
    },
    listItemLabelStyle: {
        fontFamily: ThemeUtils.FontStyle.regular,
        color: Color.BLACK,
    },
    vwInputPhotos: {
        backgroundColor: 'transparent',
        paddingVertical: 5,
        paddingHorizontal: 10,
        // height : ThemeUtils.relativeWidth(20),
        // width :  ThemeUtils.relativeWidth(20),
        borderRadius: 5,
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: Color.WHITE,
        justifyContent: 'center',
        alignItems: 'center',
    },
    vwUploadPhotosLabel: {
        backgroundColor: Color.PHOTO_BOX,
        paddingVertical: 5,
        paddingHorizontal: 10,
        marginBottom: 15,
    },
    cookBookImageStyle: {
        flex: 1,
        width: undefined,
        height: undefined,
    },
    vwCookBooksContent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    createButtonStyle: {
        flex: 1,
    },
});

