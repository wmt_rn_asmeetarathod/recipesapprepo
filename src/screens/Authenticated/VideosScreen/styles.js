import {StyleSheet} from 'react-native';
import { Color, ThemeUtils } from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    flatListStyle: {
        paddingTop: 20,
        // paddingHorizontal: 20,
        height: ThemeUtils.relativeWidth(80),
        flex: 1,
    },
    contentContainerStyle: {
        flexGrow: 1,
        paddingBottom: 30,
        alignSelf : 'center',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        opacity: 0.9,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(30),
        width: ThemeUtils.relativeWidth(40),
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexGrow: 1,
    },
    itemStyle: {
        width: ThemeUtils.relativeWidth(90),
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        borderRadius: 20,
        backgroundColor: Color.WHITE,
        paddingVertical: 20,
    },

    titleStyle: {
        paddingBottom: 10,
        alignSelf: 'center',
        fontFamily: 'KoHo-Bold',
    },
});
