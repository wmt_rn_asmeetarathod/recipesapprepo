import React, {useState, useEffect} from 'react';
import {View, FlatList, ImageBackground} from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';
import {styles} from './styles';
import {Label, NoInternetModal} from 'src/component';
import {Color, ThemeUtils, isNetworkConnected} from 'src/utils';
import videoLinks from 'src/utils/videoLinks';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';

const Videos = (props) => {
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);

    /*  Life-cycles Methods */
    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    const checkNetwork = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setNoInternetModalVisible(false);
            }
        });
    };

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const {strYoutube, strMeal, idMeal} = itemData;

        return (
            <View style={styles.itemStyle}>
                <Label style={styles.titleStyle} color={Color.CELLO}>
                    {strMeal}
                </Label>
                <YoutubePlayer
                    height={ThemeUtils.relativeWidth(50)}
                    width={ThemeUtils.relativeWidth(80)}
                    play={false}
                    apiKey={'AIzaSyDg8PlwLlfRIa4Rp0_ogXERFpN9FxRWrSU'}
                    videoId={strYoutube}
                />
            </View>
        );
    };

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={checkNetwork}
                />
            ) : (
                <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}>
                    <View style={styles.container}>
                        <FlatList
                            style={styles.flatListStyle}
                            contentContainerStyle={styles.contentContainerStyle}
                            data={videoLinks}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.idMeal.toString()}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </ImageBackground>
            )}
        </>
    );
};

export default Videos;
