import React, {useEffect, useState} from 'react';

import {
    View,
    FlatList,
    Image,
    ImageBackground,
    Dimensions,
    Modal,
} from 'react-native';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {styles} from './styles';
import {Label, Ripple, MaterialTextInput, NoInternetModal} from 'src/component';
import {APIRequest} from 'src/api';
import {Color, Strings, isNetworkConnected} from 'src/utils';
import Routes from 'src/router/Routes';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';
import APP_BACKGROUND4 from 'src/assets/images/APP_BACKGROUND4.jpg';

const CategoryList = (props) => {
    const [data, setData] = useState();
    const [modalVisible, setModalVisible] = useState(false);
    const [searchText, setSearchText] = useState();
    const [searchedData, setSearchedData] = useState();
    const [filtersVisible, setFiltersVisible] = useState(true);
    const [areas, setAreas] = useState([]);
    const [categories, setCategories] = useState([]);
    const [searchArea, setSearchArea] = useState();
    const [searchCategory, setSearchCategory] = useState();
    const [tags, setTags] = useState([]);
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [indicatorVisible, setIndicatorVisible] = useState(false);

    /*  Life-cycles Methods */
    useEffect(() => {
        getAllApiData();
    }, []);

    const onPressTryAgain = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(true);
                getAllApiData();
            }
        });
    };

    const getAllApiData = () => {      
        getCategories();
        getAreaList();
        getCategoryList();
    };

    const getCategories = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL('https://www.themealdb.com/api/json/v1/1/categories.php')
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();
    };

    const onResponse = (response) => {
        setData(response.data.categories);
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(false);
                setNoInternetModalVisible(false);
            }
        });
    };
    const onError = (error) => {
        console.log('error ==>> ', error);
        if (error.status === 522) {
            console.log('error.status ==>>> ', error.status);
            setNoInternetModalVisible(true);
        }
    };

    /*  UI Events Methods   */
    const isPortrait = () => {
        const dim = Dimensions.get('window');
        return dim.height >= dim.width;
    };

    const [orientation, setOrientation] = useState(
        isPortrait() ? 'portrait' : 'landscape',
    );

    Dimensions.addEventListener('change', () => {
        setOrientation(isPortrait() ? 'portrait' : 'landscape');
    });

    const goToFoodList = (strCategory) => {
        props.navigation.navigate(Routes.FoodList, {categoryName: strCategory});
    };

    const searchFilterFunction = (text) => {
        setSearchText(text);
        if (text.length !== 0) {
            getSearchedData(text);
        }
        if (text.length === 0) {
            setSearchedData([]);
        }
    };

    const getSearchedData = (searchValue) => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                `https://www.themealdb.com/api/json/v1/1/search.php?s=${searchValue}`,
            )
            .response(onSearchResponse)
            .error(onSearchError)
            .build()
            .doRequest();
    };

    const onSearchResponse = (response) => {
        setSearchedData(response.data.meals);
    };
    const onSearchError = (error) => {
        console.log('onSearchError error ==>> ', error);
    };

    const goToFoodDetail = (idMeal, strMeal) => {
        setModalVisible(false);
        props.navigation.navigate(Routes.FoodDetail, {
            itemId: idMeal,
            itemName: strMeal,
            fromApi: true,
        });
    };

    const handleClose = () => {
        setSearchArea(null);
        setSearchCategory(null);
        if (searchText !== '') {
            setSearchText('');
        } else {
            setModalVisible(false);
            setSearchedData([]);
            setFiltersVisible(true);
        }
    };

    const getAreaList = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(`https://www.themealdb.com/api/json/v1/1/list.php?a=list`)
            .response(onAreaResponse)
            .error(onAreaError)
            .build()
            .doRequest();
    };

    const onAreaResponse = (response) => {
        let areasArr = [];
        areasArr = response.data.meals;
        areasArr.map(({strArea}) => {
            areas.push(strArea);
        });
        setAreas(areas);
    };
    const onAreaError = (error) => {
        console.log('Area error ==>> ', error);
        if (error.status === 522) {
            console.log('error.status ==>>> ', error.status);
            setNoInternetModalVisible(true);
        }
    };

    const getCategoryList = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(`https://www.themealdb.com/api/json/v1/1/list.php?c=list`)
            .response(onCategoryResponse)
            .error(onCategoryError)
            .build()
            .doRequest();
    };

    const onCategoryResponse = (response) => {
        let categoriesArr = [];
        categoriesArr = response.data.meals;
        categoriesArr.map(({strCategory}) => {
            categories.push(strCategory);
        });
        setCategories(categories);
    };
    const onCategoryError = (error) => {
        console.log('Category error ==>> ', error);
        if (error.status === 522) {
            console.log('error.status ==>>> ', error.status);
            setNoInternetModalVisible(true);
        }
    };

    const handleSearchArea = (area) => {
        setSearchArea(area);
        tags.push(area);
        setTags(tags);
    };

    const handleSearchCategory = (category) => {
        setSearchCategory(category);
        tags.push(category);
        setTags(tags);
    };

    const clearArea = () => {
        setSearchArea(null);
    };

    const clearCategory = () => {
        setSearchCategory(null);
    };

    const displayFilters = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                `https://www.themealdb.com/api/json/v1/1/filter.php?a=${
                    searchArea && searchArea
                }&c=${searchCategory && searchCategory}`,
            )
            .response(onFilterResponse)
            .error(onFilterError)
            .build()
            .doRequest();
    };

    const onFilterResponse = (response) => {
        setFiltersVisible(false);
        setSearchedData(response.data.meals);
    };

    const onFilterError = (error) => {
        setFiltersVisible(false);
        console.log('onFilterError error ==>> ', error);
    };

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const {strCategoryThumb, strCategory} = itemData;

        return (
            <View>
                <Ripple
                    rippleContainerBorderRadius={0}
                    style={styles.itemStyle}
                    onPress={() => goToFoodList(strCategory)}>
                    <Image
                        source={{uri: strCategoryThumb}}
                        style={styles.foodImageStyle}
                    />
                    <Label style={styles.titleStyle} color={Color.WHITE}>
                        {strCategory}
                    </Label>
                </Ripple>
            </View>
        );
    };

    const RenderSearchedDataCompo = ({itemData}) => {
        const {strMealThumb, strMeal, idMeal} = itemData;

        return (
            <View>
                <Ripple
                    rippleContainerBorderRadius={0}
                    style={styles.sItemStyle}
                    onPress={() => goToFoodDetail(idMeal, strMeal)}>
                    <Label
                        style={styles.sTitleStyle}
                        color={Color.CELLO}
                        numberOfLines={2}>
                        {strMeal}
                    </Label>
                    <Image
                        source={{uri: strMealThumb}}
                        style={styles.sFoodImageStyle}
                    />
                </Ripple>
            </View>
        );
    };

    return (
        <>
            {modalVisible && (
                <View>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={modalVisible}
                        onRequestClose={() => setModalVisible(false)}>
                        <ImageBackground
                            style={styles.backgroundImage}
                            source={APP_BACKGROUND4}>
                            <View style={styles.searchMaincontainer}>
                                <View style={styles.serchContainer}>
                                    <View style={styles.vwInput}>
                                        <MaterialTextInput
                                            value={searchText}
                                            style={styles.inputStyle}
                                            autoFocus
                                            onChangeText={(text) =>
                                                searchFilterFunction(text)
                                            }
                                            placeholder={Strings.searchRecipes}
                                            placeholderTextColor={Color.WHITE}
                                            lineWidth={0}
                                            activeLineWidth={0}
                                            disabledLineWidth={0}
                                        />
                                    </View>
                                    <Ripple
                                        rippleContainerBorderRadius={50}
                                        onPress={() => displayFilters()}
                                        style={styles.searchIconStyle}>
                                        <IonIcon
                                            name="funnel-outline"
                                            style={styles.searchIcon}
                                            color={Color.WHITE}
                                            size={25}
                                        />
                                    </Ripple>
                                    <Ripple
                                        rippleContainerBorderRadius={50}
                                        onPress={() => handleClose()}
                                        style={styles.searchIconStyle}>
                                        <IonIcon
                                            name="close-outline"
                                            style={styles.searchIcon}
                                            color={Color.WHITE}
                                            size={25}
                                        />
                                    </Ripple>
                                </View>
                                {!searchText && (
                                    <>
                                        <View style={styles.vwFilteredLabel}>
                                            {searchArea && (
                                                <View style={styles.vwTagStyle}>
                                                    <Label
                                                        color={Color.WHITE}
                                                        small>
                                                        {searchArea}
                                                    </Label>
                                                    <Ripple
                                                        rippleContainerBorderRadius={
                                                            50
                                                        }
                                                        onPress={() =>
                                                            clearArea()
                                                        }
                                                        style={
                                                            styles.closeIconStyle
                                                        }>
                                                        <IonIcon
                                                            name="close-circle-outline"
                                                            color={Color.WHITE}
                                                            style={
                                                                styles.closeIcon
                                                            }
                                                            size={25}
                                                        />
                                                    </Ripple>
                                                </View>
                                            )}
                                            {searchCategory && (
                                                <View style={styles.vwTagStyle}>
                                                    <Label
                                                        color={Color.WHITE}
                                                        small>
                                                        {searchCategory}
                                                    </Label>
                                                    <Ripple
                                                        rippleContainerBorderRadius={
                                                            50
                                                        }
                                                        onPress={() =>
                                                            clearCategory()
                                                        }
                                                        style={
                                                            styles.closeIconStyle
                                                        }>
                                                        <IonIcon
                                                            name="close-circle-outline"
                                                            color={Color.WHITE}
                                                            style={
                                                                styles.closeIcon
                                                            }
                                                            size={25}
                                                        />
                                                    </Ripple>
                                                </View>
                                            )}
                                        </View>
                                        {filtersVisible && (
                                            <Label
                                                color={Color.WHITE}
                                                font_bold
                                                style={styles.areaLabel}>
                                                {'Area'}
                                            </Label>
                                        )}

                                        <View style={styles.vwFilterArea}>
                                            {filtersVisible &&
                                                /* Area Labels */
                                                areas.map((area, index) => {
                                                    if (index < 10) {
                                                        return (
                                                            <Ripple
                                                                key={area}
                                                                rippleContainerBorderRadius={
                                                                    50
                                                                }
                                                                style={
                                                                    styles.vwAreaLabelStyle
                                                                }
                                                                onPress={() =>
                                                                    handleSearchArea(
                                                                        area,
                                                                    )
                                                                }>
                                                                <Label
                                                                    color={
                                                                        Color.WHITE
                                                                    }
                                                                    small>
                                                                    {area}
                                                                </Label>
                                                            </Ripple>
                                                        );
                                                    }
                                                })}
                                        </View>
                                        {filtersVisible && (
                                            <Label
                                                color={Color.WHITE}
                                                font_bold
                                                style={styles.areaLabel}>
                                                {'Category'}
                                            </Label>
                                        )}
                                        <View style={styles.vwFilterArea}>
                                            {filtersVisible &&
                                                /* Categories Labels */
                                                categories.map(
                                                    (category, index) => {
                                                        if (index < 10) {
                                                            return (
                                                                <Ripple
                                                                    key={
                                                                        category
                                                                    }
                                                                    rippleContainerBorderRadius={
                                                                        50
                                                                    }
                                                                    style={
                                                                        styles.vwAreaLabelStyle
                                                                    }
                                                                    onPress={() =>
                                                                        handleSearchCategory(
                                                                            category,
                                                                        )
                                                                    }>
                                                                    <Label
                                                                        color={
                                                                            Color.WHITE
                                                                        }
                                                                        small>
                                                                        {
                                                                            category
                                                                        }
                                                                    </Label>
                                                                </Ripple>
                                                            );
                                                        }
                                                    },
                                                )}
                                        </View>
                                    </>
                                )}

                                <FlatList
                                    style={styles.searchedRecipeListStyle}
                                    data={searchedData}
                                    renderItem={({item}) => (
                                        <RenderSearchedDataCompo
                                            itemData={item}
                                        />
                                    )}
                                    keyExtractor={(item) =>
                                        item.idMeal.toString()
                                    }
                                />
                            </View>
                        </ImageBackground>
                    </Modal>
                </View>
            )}

            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}>
                    <View style={styles.container}>
                        <View style={styles.serchContainer}>
                            <View style={styles.vwInput}>
                                <Label
                                    onPress={() => setModalVisible(true)}
                                    style={styles.inputViewStyle}
                                    color={Color.WHITE}>
                                    {Strings.searchRecipes}
                                </Label>
                            </View>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => displayFilters()}
                                style={styles.searchIconStyle}>
                                <IonIcon
                                    name="funnel-outline"
                                    style={styles.searchIcon}
                                    color={Color.WHITE}
                                    size={25}
                                />
                            </Ripple>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleClose()}
                                style={styles.searchIconStyle}>
                                <IonIcon
                                    name="close-outline"
                                    style={styles.searchIcon}
                                    color={Color.WHITE}
                                    size={25}
                                />
                            </Ripple>
                        </View>

                        <FlatList
                            style={styles.flatListStyle}
                            contentContainerStyle={{
                                flexGrow: 1,
                                paddingBottom: 30,
                            }}
                            data={data}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.idCategory.toString()}
                            key={orientation}
                            showsVerticalScrollIndicator={false}
                            numColumns={orientation === 'portrait' ? 2 : 4}
                        />
                    </View>
                </ImageBackground>
            )}
        </>
    );
};

export default CategoryList;
