import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flatListStyle: {
        paddingTop: 30,
        paddingHorizontal: 30,
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'contain',
        zIndex: -1,
        width: null,
        height: null,
        // opacity:0.7,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(30),
        width: ThemeUtils.relativeWidth(40),
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexGrow: 1,
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(50),
        width: ThemeUtils.relativeWidth(40),
        justifyContent: 'space-between',
        marginHorizontal: 5,
        marginBottom: 20,
        borderRadius: 20,
        backgroundColor: Color.WHITE,
    },

    titleStyle: {
        backgroundColor: Color.CELLO,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 15,
        textAlign: 'center',
    },
    searchViewStyle: {},
    searchMaincontainer: {
        flex: 1,
        justifyContent: 'center',
    },
    searchIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginStart: 5,
        alignSelf: 'flex-end',
    },
    closeIcon: {},
    searchIcon: {
        marginStart: 10,
        marginEnd: 10,
    },
    serchContainer: {
        flexDirection: 'row',
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        height: ThemeUtils.APPBAR_HEIGHT - 10,
        borderColor: Color.WHITE,
        borderWidth: 1,
        borderRadius: 30,
        marginTop: 10,
        marginHorizontal: 10,
    },
    vwInput: {
        flex: 1,
        height: ThemeUtils.APPBAR_HEIGHT,
        justifyContent: 'center',
    },
    inputStyle: {
        alignSelf: 'center',
        paddingStart: 10,
        color: Color.WHITE,
    },
    inputViewStyle: {
        paddingStart: 10,
        color: Color.WHITE,
    },
    /* Searched Recipes List Style */
    searchedRecipeListStyle: {
        paddingTop: 30,
        paddingHorizontal: 20,
        flex: 1,
    },
    sFoodImageStyle: {
        height: ThemeUtils.relativeWidth(20),
        width: ThemeUtils.relativeWidth(30),
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        alignSelf: 'flex-end',
    },
    sItemStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 5,
        marginVertical: 8,
        borderRadius: 15,
        backgroundColor: Color.WHITE,
        maxHeight: ThemeUtils.relativeWidth(20),
    },
    sTitleStyle: {
        paddingStart: 20,
        flexWrap: 'wrap',
        width: ThemeUtils.relativeWidth(40),
    },
    vwFilterArea: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    vwAreaLabelStyle: {
        backgroundColor: Color.CELLO,
        borderRadius: 20,
        paddingVertical: 5,
        paddingHorizontal: 20,
        margin: 5,
    },
    vwTagStyle: {
        backgroundColor: Color.CELLO,
        borderRadius: 20,
        paddingVertical: 5,
        paddingHorizontal: 10,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    areaLabel: {
        paddingHorizontal: 10,
    },
    vwFilteredLabel: {
        height: ThemeUtils.APPBAR_HEIGHT - 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginVertical: 5,
    },
});
