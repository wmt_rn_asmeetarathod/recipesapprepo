import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    scrollViewStyle: {
        flex: 1,
        backgroundColor: Color.WHITE
    },
    container: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        padding: 20,
        backgroundColor: Color.WHITE
    },
    flatListStyle: {
        paddingTop: 30,
        paddingHorizontal: 30,
        flex: 1
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        // opacity:0.7,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(30),
        width: ThemeUtils.relativeWidth(30),
        resizeMode: 'contain',
        borderRadius: ThemeUtils.relativeWidth(15)
    },
    itemStyle: {
        height: ThemeUtils.relativeWidth(50),
        width: ThemeUtils.relativeWidth(40),
        justifyContent: 'space-between',
        marginHorizontal: 5,
        marginBottom: 20,
        borderRadius: 20,
        backgroundColor: Color.WHITE
    },

    titleStyle: {
        backgroundColor: Color.CELLO,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 15
    },
    vwUpper: {
        flexDirection: 'row',
        height: ThemeUtils.relativeHeight(18),
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    vwCounter: {
        flexDirection: 'row',
        backgroundColor: Color.WHITE,
        justifyContent: 'space-around',
        alignItems: 'center',
        // padding: 10,
        marginVertical: 20,
        elevation: 5
    },
    vwNameTitle: {
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    vwToggleButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 5
    },
    toggleButtonStyle: {
        flex: 1,
        alignItems: 'center',
        padding: 10
    },
    activeButtonStyle: {
        backgroundColor: Color.CELLO
    },
    inActiveButtonStyle: {
        backgroundColor: Color.WHITE
    },
    marginVertical: {
        marginVertical: 20
    },
    vwCookBooksContent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    /* Header Style */
    headerStyle: {
        height: ThemeUtils.APPBAR_HEIGHT,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 0,
        backgroundColor: Color.WHITE,
        elevation: 0
    },
    headerLeftIcon: {
        position: 'absolute',
        left: ThemeUtils.relativeWidth(2)
    },
    headerRightIcon: {
        position: 'absolute',
        right: ThemeUtils.relativeWidth(2)
    },
    headerRightAddIcon: {
        position: 'absolute',
        right: ThemeUtils.relativeWidth(12)
    },
    headerTitle: {
        textAlign: 'center',
        justifyContent: 'center',
        color: Color.CELLO,
        fontFamily: ThemeUtils.FontStyle.bold
    },
    vwFollowBtn: {
        alignItems: 'center'
    },
    followButtonStyle: {},
    unFollowButtonStyle: {
        backgroundColor: 'red',
        borderRadius: 20
    },
    cookBookImageStyle: {
        flex: 1,
        width: undefined,
        height: undefined
    },
    labelStyle:{
        flex :1,
        alignSelf : 'center',
    },
    counterLabelStyle:{
        alignItems : 'center',
        padding: 10,
    }
});

