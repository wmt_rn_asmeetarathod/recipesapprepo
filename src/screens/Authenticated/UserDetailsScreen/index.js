import React, {useEffect, useState} from 'react';
import {View, Image, Dimensions, ScrollView, Alert, Button} from 'react-native';
import Routes from 'src/router/Routes';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import {styles} from './styles';
import {Label, Ripple, Hr, RoundButton, NoInternetModal} from 'src/component';
import {
    CommonStyle,
    Strings,
    Color,
    ThemeUtils,
    isNetworkConnected,
} from 'src/utils';
import USER_PLACEHOLDER_IMAGE from 'src/assets/images/user_placeholder_image.png';

const UserDetails = (props) => {
    const {otherUserUid} = props.route.params;
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);

    const user = auth().currentUser;
    const [active, setActive] = useState('cookbook');
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const [userFullName, setUserFullName] = useState();
    const [userProfilePhoto, setUserProfilePhoto] = useState();
    const [follow, setFollow] = useState(false);
    const [ownName, setOwnName] = useState();
    const [ownProfilePhoto, setOwnProfilePhoto] = useState();
    const [following, setFollowing] = useState();
    const [followers, setFollowers] = useState();
    const [recipeData, setRecipeData] = useState([]);
    const [indicatorVisible, setIndicatorVisible] = useState(false);

    /*  Life-cycles Methods */
    useEffect(() => {
        isNetworkConnected().then((res) => {
            if (!res) {
                setNoInternetModalVisible(true);
            }
        });
    }, []);

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            getData();
        }

        return () => (mounted = false);
    }, []);

    const onPressTryAgain = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(true);
                getData();
            }
        });
    };

    const getData = () => {
        database()
            .ref(`recipes`)
            .on('value', (snapshot) => {
                let recipeArray = [];
                snapshot.forEach((data) => {
                    if (data.val().userUid === otherUserUid) {
                        database()
                            .ref(`recipes/${data.key}`)
                            .on('value', (snap) => {
                                recipeArray.push(snap?.val());
                            });
                    }
                });
                setRecipeData(recipeArray);
            });

        database()
            .ref(`users/${otherUserUid}`)
            .on('value', (snapshot) => {
                setUserFullName(snapshot?.val()?.fullName);
                setUserProfilePhoto(snapshot?.val()?.photoURL);
                if (snapshot?.val()?.followingList) {
                    setFollowing(
                        Object.keys(snapshot?.val()?.followingList).length,
                    );
                } else {
                    setFollowing(0);
                }
                if (snapshot?.val()?.followerList) {
                    setFollowers(
                        Object.keys(snapshot?.val()?.followerList).length,
                    );
                } else {
                    setFollowers(0);
                }
            });
        database()
            .ref(`users/${user.uid}`)
            .on('value', (snapshot) => {
                console.log('snapshot?.val() ', snapshot?.val());
                setOwnName(snapshot?.val()?.fullName);
                setOwnProfilePhoto(snapshot?.val()?.photoURL);
                if (snapshot?.val()?.followingList) {
                    if (
                        snapshot
                            .val()
                            .followingList.hasOwnProperty(otherUserUid)
                    ) {
                        setFollow(true);
                    }
                }
            });
        isNetworkConnected().then((res) => {
            if (
                res &&
                userFullName &&
                userProfilePhoto &&
                ownName &&
                ownProfilePhoto
            ) {
                setIndicatorVisible(false);
                setNoInternetModalVisible(false);
            }
        });
    };

    const images = [
        require('../../../assets/images/food1.jpg'),
        require('../../../assets/images/food2.jpg'),
        require('../../../assets/images/food3.jpg'),
        require('../../../assets/images/food4.jpg'),
        require('../../../assets/images/food5.jpg'),
        require('../../../assets/images/food6.jpg'),
        require('../../../assets/images/food7.jpg'),
        require('../../../assets/images/food8.jpg'),
        require('../../../assets/images/food9.jpg'),
        require('../../../assets/images/food10.jpg'),
        require('../../../assets/images/food11.jpg'),
        require('../../../assets/images/food12.jpg'),
        require('../../../assets/images/food13.jpeg'),
        require('../../../assets/images/food14.jpeg'),
        require('../../../assets/images/food15.jpeg'),
    ];

    const handleOnPressFollow = () => {
        setFollow(!follow);
        if (!follow) {
            database()
                .ref(`users/${user.uid}/followingList/${otherUserUid}`)
                .set({
                    userUid: otherUserUid,
                    userFullName: userFullName,
                    userProfilePhotoUrl: userProfilePhoto,
                });
            database()
                .ref(`users/${otherUserUid}/followerList/${user.uid}`)
                .set({
                    userUid: user.uid,
                    userFullName: ownName,
                    userProfilePhotoUrl: ownProfilePhoto,
                });
        } else {
            database()
                .ref(`users/${user.uid}/followingList/${otherUserUid}`)
                .remove();
            database()
                .ref(`users/${otherUserUid}/followerList/${user.uid}`)
                .remove();
        }
    };

    const goToFollowersList = () => {
        props.navigation.navigate(Routes.UserList, {
            title: Strings.followers,
            userUid: otherUserUid,
        });
    };

    const goToFollowingList = () => {
        props.navigation.navigate(Routes.UserList, {
            title: Strings.following,
            userUid: otherUserUid,
        });
    };

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                <ScrollView
                    style={styles.scrollViewStyle}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.container}>
                        <View style={styles.vwUpper}>
                            <Image
                                source={
                                    userProfilePhoto
                                        ? {uri: userProfilePhoto}
                                        : USER_PLACEHOLDER_IMAGE
                                }
                                style={styles.foodImageStyle}
                            />
                            <View style={styles.vwNameTitle}>
                                <Label font_bold large>
                                    {userFullName && userFullName}
                                </Label>
                                <Label>{'Food Innovator'}</Label>
                            </View>
                        </View>
                        <View style={styles.vwCounter}>
                            <View style={styles.counterLabelStyle}>
                                <Label font_bold large>
                                    {recipeData.length}
                                </Label>
                                <Label>{Strings.recipes}</Label>
                            </View>
                            <Ripple onPress={() => goToFollowingList()}>
                                <View style={styles.counterLabelStyle}>
                                    <Label font_bold large>
                                        {following}
                                    </Label>
                                    <Label> {Strings.following}</Label>
                                </View>
                            </Ripple>

                            <Ripple onPress={() => goToFollowersList()}>
                                <View style={styles.counterLabelStyle}>
                                    <Label font_bold large>
                                        {followers}
                                    </Label>
                                    <Label>{Strings.followers}</Label>
                                </View>
                            </Ripple>
                        </View>

                        <View style={styles.vwFollowBtn}>
                            <RoundButton
                                btn_xs
                                click={handleOnPressFollow}
                                width={ThemeUtils.relativeWidth(90)}
                                mt={ThemeUtils.relativeRealHeight(0)}
                                backgroundColor={
                                    follow ? Color.WHITE : Color.BLUE
                                }
                                borderColor={follow ? Color.BLACK : null}
                                borderWidth={1}
                                textColor={follow ? Color.BLACK : Color.WHITE}
                                style={styles.labelStyle}>
                                {' '}
                                {follow ? 'UNFOLLOW' : 'FOLLOW'}
                            </RoundButton>
                        </View>
                        <View style={styles.marginVertical}>
                            <Hr />
                        </View>
                        <View style={styles.vwCookBooksContent}>
                            {recipeData.map((recipe, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={[
                                            {
                                                width: width / 3 - 15,
                                                height: width / 3 - 15,
                                                marginBottom: 2,
                                                paddingLeft:
                                                    index % 3 !== 0 ? 2 : 0,
                                            },
                                        ]}>
                                        <Image
                                            style={styles.cookBookImageStyle}
                                            source={{uri: recipe.recipePhoto}}
                                        />
                                    </View>
                                );
                            })}
                        </View>
                    </View>
                </ScrollView>
            )}
        </>
    );
};

export default UserDetails;
