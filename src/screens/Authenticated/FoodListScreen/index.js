import React, {useEffect, useState} from 'react';
import {View, FlatList, Image, ImageBackground} from 'react-native';
import {styles} from './styles';
import {Label, Ripple, NoInternetModal} from 'src/component';
import {Color, isNetworkConnected} from 'src/utils';
import {APIRequest} from 'src/api';
import Routes from 'src/router/Routes';
import APP_BACKGROUND from 'src/assets/images/APP_BACKGROUND.jpeg';

const FoodList = (props) => {
    const [data, setData] = useState();
    const {categoryName} = props.route.params;
    const [noInternetModalVisible, setNoInternetModalVisible] = useState(false);
    const [indicatorVisible, setIndicatorVisible] = useState(false);

    /*  Life-cycles Methods */

    useEffect(() => {
        getData();
    }, []);

    const onPressTryAgain = () => {
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(true);
                getData();
            }
        });
    };

    const getData = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(1)
            .reqURL(
                `https://www.themealdb.com/api/json/v1/1/filter.php?c=${categoryName}`,
            )
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();
    };

    const onResponse = (response) => {
        console.log('Foodlist data ==>> ', response);
        setData(response.data.meals);
        isNetworkConnected().then((res) => {
            if (res) {
                setIndicatorVisible(false);
                setNoInternetModalVisible(false);
            }
        });
    };

    const onError = (error) => {
        console.log('Foodlist error ==>> ', error);
        if (error.status === 522) {
            console.log('error.status ==>>> ', error.status);
            setNoInternetModalVisible(true);
        }
    };

    /*  UI Events Methods   */
    const goToFoodDetail = (idMeal, strMeal) => {
        props.navigation.navigate(Routes.FoodDetail, {
            itemId: idMeal,
            itemName: strMeal,
            fromApi: true,
        });
    };

    /*  Custom-Component sub-render Methods */
    const RenderItemCompo = ({itemData}) => {
        const {strMealThumb, strMeal, idMeal} = itemData;

        return (
            <View>
                <Ripple
                    rippleContainerBorderRadius={0}
                    style={styles.itemStyle}
                    onPress={() => goToFoodDetail(idMeal, strMeal)}>
                    <Label
                        style={styles.titleStyle}
                        color={Color.CELLO}
                        numberOfLines={2}>
                        {strMeal}
                    </Label>
                    <Image
                        source={{uri: strMealThumb}}
                        style={styles.foodImageStyle}
                    />
                </Ripple>
            </View>
        );
    };

    return (
        <>
            {noInternetModalVisible ? (
                <NoInternetModal
                    show={noInternetModalVisible}
                    onPress={onPressTryAgain}
                    visible={indicatorVisible}
                />
            ) : (
                <ImageBackground
                    style={styles.backgroundImage}
                    source={APP_BACKGROUND}>
                    <View style={styles.container}>
                        <FlatList
                            style={styles.flatListStyle}
                            contentContainerStyle={{paddingBottom: 50}}
                            data={data}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item) => item.idMeal.toString()}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </ImageBackground>
            )}
        </>
    );
};

export default FoodList;
