import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    flatListStyle: {
        paddingTop: 30,
        paddingHorizontal: 10,
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        zIndex: -1,
        width: null,
        height: null,
        opacity: 1,
    },
    foodImageStyle: {
        height: ThemeUtils.relativeWidth(20),
        width: ThemeUtils.relativeWidth(30),
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        alignSelf: 'flex-end',
    },
    itemStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 5,
        marginVertical: 8,
        borderRadius: 15,
        backgroundColor: Color.WHITE,
        maxHeight: ThemeUtils.relativeWidth(20),
    },
    titleStyle: {
        paddingStart: 20,
        flexWrap: 'wrap',
        width: ThemeUtils.relativeWidth(40),
    },
});
