/**
 * Created using React-Native Base
 * https://webmobtech.com
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';

import {PersistGate} from 'redux-persist/integration/react';
import {connect, Provider} from 'react-redux';
import {persistor, store} from './src/redux/store';

import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Toast} from 'src/component';
import {Color} from 'src/utils';
import RootNavigator from 'src/router';
import {changeNetworkStatus} from './src/redux/action';
import NetInfo from '@react-native-community/netinfo';

const MyAppTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: Color.PRIMARY,
        background: Color.PRIMARY_BACKGROUND,
        card: Color.PRIMARY,
        text: Color.WHITE,
    },
};

const App: () => React$Node = (props) => {

    const [offline, setOffline] = useState(false);

    useEffect(() => {
        const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
            console.log('Connection type - ', state.type);
            console.log('Is connected? - ', state.isConnected);
            const offline = !(state.isConnected && state.isInternetReachable);
            props.changeNetworkStatus(state.isConnected);
            setOffline(offline);
        });

        return () => removeNetInfoSubscription();
    }, []);

    return (
        <PersistGate loading={null} persistor={persistor}>
            <SafeAreaProvider>
                <StatusBar barStyle="default" backgroundColor={Color.CELLO} />
                <NavigationContainer theme={MyAppTheme}>
                    <RootNavigator />
                </NavigationContainer>
            </SafeAreaProvider>
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </PersistGate>
    );
};

const mapStateToProps = (state) => {
    return {user: state.user, token: state.token};
};

export default connect(mapStateToProps, {changeNetworkStatus})(App);
